-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Már 31. 06:25
-- Kiszolgáló verziója: 10.3.15-MariaDB
-- PHP verzió: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `mavor`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `answer` varchar(256) NOT NULL,
  `answer_correct` tinyint(1) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `answers`
--

INSERT INTO `answers` (`id`, `answer`, `answer_correct`, `question_id`) VALUES
(1195, 'helyes válasz egy', 1, 300),
(1196, 'rossz válasz egy', 0, 300),
(1197, 'rossz válasz egy', 0, 300),
(1198, 'rossz válasz egy\r\n', 0, 300),
(1199, 'helyes válasz kettő', 1, 301),
(1200, 'rossz válasz kettő', 0, 301),
(1201, 'rossz válasz kettő', 0, 301),
(1202, 'rossz válasz kettő\r\n', 0, 301),
(1203, 'helyes válasz három', 1, 302),
(1204, 'rossz válasz három', 0, 302),
(1205, 'rossz válasz három', 0, 302),
(1206, 'rossz válasz három\r\n', 0, 302),
(1207, 'helyes válasz egy', 1, 303),
(1208, 'rossz válasz egy', 0, 303),
(1209, 'rossz válasz egy', 0, 303),
(1210, 'rossz válasz egy\r\n', 0, 303),
(1211, 'helyes válasz kettő', 1, 304),
(1212, 'rossz válasz kettő', 0, 304),
(1213, 'rossz válasz kettő', 0, 304),
(1214, 'rossz válasz kettő\r\n', 0, 304),
(1215, 'helyes válasz három', 1, 305),
(1216, 'rossz válasz három', 0, 305),
(1217, 'rossz válasz három', 0, 305),
(1218, 'rossz válasz három\r\n', 0, 305),
(1219, 'helyes válasz egy', 1, 306),
(1220, 'rossz válasz egy', 0, 306),
(1221, 'rossz válasz egy', 0, 306),
(1222, 'rossz válasz egy\r\n', 0, 306),
(1223, 'helyes válasz kettő', 1, 307),
(1224, 'rossz válasz kettő', 0, 307),
(1225, 'rossz válasz kettő', 0, 307),
(1226, 'rossz válasz kettő\r\n', 0, 307),
(1227, 'helyes válasz három', 1, 308),
(1228, 'rossz válasz három', 0, 308),
(1229, 'rossz válasz három', 0, 308),
(1230, 'rossz válasz három\r\n', 0, 308),
(1231, 'helyes válasz egy', 1, 309),
(1232, 'rossz válasz egy', 0, 309),
(1233, 'rossz válasz egy', 0, 309),
(1234, 'rossz válasz egy\r\n', 0, 309),
(1235, 'helyes válasz kettő', 1, 310),
(1236, 'rossz válasz kettő', 0, 310),
(1237, 'rossz válasz kettő', 0, 310),
(1238, 'rossz válasz kettő\r\n', 0, 310),
(1239, 'helyes válasz három', 1, 311),
(1240, 'rossz válasz három', 0, 311),
(1241, 'rossz válasz három', 0, 311),
(1242, 'rossz válasz három\r\n', 0, 311);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `credit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `courses`
--

INSERT INTO `courses` (`id`, `course_name`, `credit`) VALUES
(80, 'Próba', 10),
(81, 'fda', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `course_contents`
--

CREATE TABLE `course_contents` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `course_contents`
--

INSERT INTO `course_contents` (`id`, `course_id`, `video_id`) VALUES
(131, 80, 138),
(132, 80, 139),
(133, 80, 140),
(134, 81, 141),
(135, 81, 142);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `video_id` int(11) DEFAULT NULL,
  `question` varchar(256) NOT NULL,
  `question_type` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `questions`
--

INSERT INTO `questions` (`id`, `course_id`, `video_id`, `question`, `question_type`) VALUES
(300, 80, NULL, 'Lorem?2', 'block'),
(301, 80, NULL, 'ipsum?2', 'block'),
(302, 80, NULL, 'dolor?2', 'block'),
(303, 80, NULL, 'sit amet?2', 'final'),
(304, 80, NULL, 'sit amet?2', 'final'),
(305, 80, NULL, 'sit amet?2', 'final'),
(306, 81, NULL, 'Lorem?2', 'block'),
(307, 81, NULL, 'ipsum?2', 'block'),
(308, 81, NULL, 'dolor?2', 'block'),
(309, 81, NULL, 'sit amet?2', 'final'),
(310, 81, NULL, 'sit amet?2', 'final'),
(311, 81, NULL, 'sit amet?2', 'final');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `question_video`
--

CREATE TABLE `question_video` (
  `id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `super_user`
--

CREATE TABLE `super_user` (
  `id` int(11) NOT NULL,
  `su_name` varchar(256) NOT NULL,
  `su_pass` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `super_user`
--

INSERT INTO `super_user` (`id`, `su_name`, `su_pass`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `video_name` varchar(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `videos`
--

INSERT INTO `videos` (`id`, `video_name`, `url`, `position`) VALUES
(138, 'css_animation_tutorial_#11___animating_a_pop_up', 'assets/videos/css_animation_tutorial_#11___animating_a_pop_up.mp4', 3),
(139, 'css_animation_tutorial_#2___transforms', 'assets/videos/css_animation_tutorial_#2___transforms.mp4', 2),
(140, 'css_animation_tutorial_#1___introduction', 'assets/videos/css_animation_tutorial_#1___introduction.mp4', 1),
(141, 'css_animation_tutorial_#2___transforms', 'assets/videos/css_animation_tutorial_#2___transforms.mp4', 1),
(142, 'css_animation_tutorial_#1___introduction', 'assets/videos/css_animation_tutorial_#1___introduction.mp4', 2);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- A tábla indexei `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `course_contents`
--
ALTER TABLE `course_contents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_content_ibfk_1` (`course_id`),
  ADD KEY `course_content_ibfk_2` (`video_id`);

--
-- A tábla indexei `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`);

--
-- A tábla indexei `question_video`
--
ALTER TABLE `question_video`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `super_user`
--
ALTER TABLE `super_user`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1243;

--
-- AUTO_INCREMENT a táblához `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT a táblához `course_contents`
--
ALTER TABLE `course_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT a táblához `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;

--
-- AUTO_INCREMENT a táblához `question_video`
--
ALTER TABLE `question_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `super_user`
--
ALTER TABLE `super_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `course_contents`
--
ALTER TABLE `course_contents`
  ADD CONSTRAINT `course_contents_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `course_contents_ibfk_2` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
