const mavor = (()=>{
    return{
        init: ()=>{
            mavor.SideBarMenuColor();
            mavor.SideBarCollapse();
            mavor.Click_video();
            // mavor.DragDropUpload();

            mavor.OrderVideos();
            mavor.CheckBoxes();
           
        },

        SideBarMenuColor: ()=>{
            var key = location.pathname.split("/").pop();
            var navs = document.querySelectorAll('.sidebar-link');
            
            navs.forEach((item)=>{
                var href_key = item.href.split("/").pop();
                href_key === key ? item.style.color = "white" : "";
            
            });


        },
        SideBarCollapse: ()=>{
                //foreign code, moving sidebar
                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                });
        },

        DragDropUpload: ()=>{
            var dropzone = document.getElementById('dropzone');

            var displayUploads = function (data) {
                var uploads = document.getElementById('uploads'),
                        anchor,
                        x;
                for (x = 0; x < data.length; x = x + 1) {
                    anchor = document.createElement('p');
                    // anchor.href = data[x].file;
                    anchor.innerText = data[x].name;
        
                    uploads.appendChild(anchor);
                }
            };

            var upload = function (files) {

                var formData = new FormData(),
                        xhr = new XMLHttpRequest(),
                        x;
                for (x = 0; x < files.length; x = x + 1) {
                    formData.append('file[]', files[x]);
                }
        
                xhr.onload = function () {
                    var data = JSON.parse(this.responseText);
                    displayUploads(data);
                };
        
                xhr.open('post', 'http://localhost/mavor/admin/videos/ajax');
                xhr.send(formData);
        
            };
        
            dropzone.ondrop = function (e) {
                e.preventDefault();
                this.className = 'dropzone';
                upload(e.dataTransfer.files);
            };
        
        
            dropzone.ondragover = function () {
                this.className = 'dropzone dragover';
                return false;
            };
        
            dropzone.ondragleave = function () {
                this.className = 'dropzone';
                return false;
            };
        },

        OrderVideos: ()=>{
            const draggables = document.querySelectorAll('.draggable');
            const hidden_inputs = document.querySelectorAll('.hidden_inputs');
            const container = document.getElementById('drag_container');
            const okbutton = document.getElementById('ok');
            let dragged_text;
            let dragged_value;
            let dropped_text;
            let dropped_value;
        if (draggables != null && hidden_inputs != null && okbutton != null) {
            
        
                    for (let i = 0; i < draggables.length; i++) {
                    
                        draggables[i].addEventListener('dragstart', ()=>{
                                draggables[i].classList.add('dragging');
                                hidden_inputs[i].classList.add('dragging');
                                //megkapja az elmozdított elem szövegét + datavalue
                                dragged_text =  draggables[i].innerText;
                                dragged_value =  draggables[i].getAttribute('data-value');
                            });
                        draggables[i].addEventListener('dragend', ()=>{
                                draggables[i].classList.remove('dragging');
                                hidden_inputs[i].classList.remove('dragging');
                            });
                        draggables[i].addEventListener('dragover', e=>{
                            //megkapja azt a szöveget + datavalue ami felett éppen van a mozdított element
                                dropped_text =  draggables[i].innerText;
                                dropped_value =  draggables[i].getAttribute('data-value');
                                e.preventDefault();
                            
                            /* console.log(item.innerText);
                                dragged.innerHTML = text2;
                                item.innerHTML = text1; */
                    
                            });

                            draggables[i].addEventListener('drop', ()=>{
                                const dragged = document.querySelectorAll('.dragging');
                                        
                                dragged[0].innerHTML = dropped_text;
                                dragged[0].setAttribute('data-value', dropped_value);
                                dragged[1].setAttribute('data-value', dropped_value);
                                dragged[1].setAttribute('value', dropped_value);
                                draggables[i].innerHTML = dragged_text;
                                draggables[i].setAttribute('data-value', dragged_value);
                                hidden_inputs[i].setAttribute('data-value', dragged_value);
                                hidden_inputs[i].setAttribute('value', dragged_value);
                            
                            });

                        
                    
                    }

                    
                okbutton.addEventListener('click', ()=>{

                    var data = [];
                    draggables.forEach(item=>{
                        var key = item.getAttribute('data-value');
                        data[key] = item.getAttribute('value');
                    });

                    console.log(data);



                    //     var options = {
                    //         'number' : 1

                    // }
                    //    $.ajax({
                    //        type: 'POST',
                    //         url: 'http://localhost/mavor/admin/videos/ajax',
                    //         data: options, 

                    //    });

                    // var options = {
                    //         'number' : 1

                    // }

                    // $.post('http://localhost/mavor/admin/videos/ajax',options, function(data){
                    //     // alert(data);
                    // });

                    // var data = [];
                    // draggables.forEach(item=>{
                    //     var key = item.getAttribute('data-value');
                    //     data[key] = item.getAttribute('value');
                    // });
                
                    // var xhttp = new XMLHttpRequest();
                    // xhttp.onreadystatechange = function() {
                    //   if (this.readyState == 4 && this.status == 200) {
                    //     //   console.log(this.responseText);
                    //       //mikor kész az adott gombnak átálítjuk a classát
                                        
                    //   }
                    // };
                    // xhttp.open("POST", "http://localhost/mavor/admin/videos/ajax", true);
                    // xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    // xhttp.send("data="+data+"&lname=Ford");
                });
        }
        },
        Click_video: ()=>{
            const src = document.querySelectorAll('source');
            const videos = document.querySelectorAll('.side-video');
            const main_video = document.querySelector('#main_video');
        if (src != null && videos != null) {
                for(let i = 0; i< videos.length; i++){
                    videos[i].addEventListener('click', ()=>{
                        main_video.setAttribute('src',src[i].getAttribute('src') );
                      
                    });
                }
        }
        
        
        },
        CheckBoxes: ()=>{

            const video_ids = document.querySelectorAll('.picked-video-id');
            const question_ids = document.querySelectorAll('.picked-question-id');
            const picked_videos = document.querySelectorAll('.picked-video');
            const picked_questions = document.querySelectorAll('.picked-question');
            const matchButton = document.querySelector('#match');
            var video_id;
            var data = [];

            // draggables.forEach(item=>{
            //     var key = item.getAttribute('data-value');
            //     data[key] = item.getAttribute('value');
            // });
            if (matchButton != null) {
                matchButton.addEventListener('click', ()=>{
                    for(let i= 0; i < video_ids.length; i++){
                        if (video_ids[i].checked == true) {
                            video_id = video_ids[i].getAttribute('value');
                            video_ids[i].style.display = "none";        
                            picked_videos[i].style.display = "none";                 
                        }
                    }

                    for(let i = 0; i< question_ids.length; i++){
                        if (question_ids[i].checked == true) {
                          
                            data.push(question_ids[i].getAttribute('value'));
                            question_ids[i].style.display = "none";
                            picked_questions[i].style.display = "none";
                        }
                    }
                   
               

                    console.log(video_id);
                    console.log(data);
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        // console.log(this.responseText);
                        var count = 0;
                       video_ids.forEach((item)=>{
                            if (item.style.display == "none") {
                              count++;

                            }

                       });  
                       if (count == video_ids.length) {
                             window.location.replace("http://localhost/mavor/admin/courses/course_added");
                       }                                      
                    }
                    };
                    xhttp.open("POST", "http://localhost/mavor/admin/courses/ajax", true);
                    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    xhttp.send("data="+data+"&video_id="+video_id);
                    
                });

            }

        }
    }
}) ();


window.onload = ()=>{
    mavor.init();
}


