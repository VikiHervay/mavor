<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends Public_Controller {
	public function __construct(){
		parent::__construct();

	}


    public function index(){
        $courses = $this->course_model->read_course();
        $data = [
            'view' => "./pages/content/course.tpl",
            'courses' => $courses
        ];
        $this->smarty->view("./admin/admin_layout.tpl", $data);
       
        
    }

    public function add(){

        if (!isset($_POST['submit_add_course1'])) {
            $data = [
             'view' => "./pages/add/add_course1.tpl",
            ];
            $this->smarty->view("./admin/admin_layout.tpl", $data);
        }else{
        
            if (empty($_POST['course_name']) || empty($_POST['course_credit']) || $_FILES['course_questions']['size'] == 0 || $_FILES['videos']['size'][0] == 0) {
                $notification = [
					"type" => "danger", 
					"label" => "Sikertelen!",
					"message" => "Minden mező kitöltése kötelező!",
					"dismissable" => TRUE
				];
           
                $videos = $this->video_model->read_video();
                $data = [
                 'course_name' => $_POST['course_name'],
                 'course_credit' => $_POST['course_credit'],
                 'notification' => $notification,
                 'view' => "./pages/add/add_course1.tpl",
                ];
                $this->smarty->view("./admin/admin_layout.tpl", $data);
            }else{
                $course_data = [
                    'course_name' => $_POST['course_name'],
                    'credit' => $_POST['course_credit']
                ];
                $course_id = $this->course_model->create_course($course_data);

                $fileDestination = 'assets/files/'.$_FILES['course_questions']['name'];
                move_uploaded_file($_FILES['course_questions']['tmp_name'], $fileDestination);  

                //uploading questions/answers into database
                $this->file_upload->HandlingExcel($fileDestination, $course_id);
                 //uploading videos video/course_content into database
                $this->file_upload->HandlingVideos($_FILES['videos'], $course_id);

               $this->add_step_2($course_id);

                 
             
                
            }

        }
     
    }

    public function add_step_2($course_id){

        if (!isset($_POST['order_videos'])) {
            $course_videos = $this->coursecontent_model->read_coursecontent('course_id', $course_id);
            $count = 1;
            foreach ($course_videos as $key => $videos) {
               $videos->video_name = $this->video_model->read_video('id',$videos->video_id);
               $videos->order = $count;
               $count++;
            }
        
            $data = [
                'view' => "./pages/add/add_course2.tpl",
                'videos' => $course_videos,
                'course_id' => $course_id
         
            ];  
                
             $this->smarty->view("./admin/admin_layout.tpl", $data);
        }else{
                $positions = $_POST['video_ids'];
                foreach ($positions as $position => $video_id) {
                    $data = [
                        'position' => $position+1  
                    ];
                    $this->video_model->update_video('id', $video_id, $data);
                }
                $this->add_step_3($course_id);
        }
       
    }

    public function add_step_3($course_id){

        if (!isset($_POST['submit'])) {
            $course_videos = $this->coursecontent_model->read_coursecontent('course_id', $course_id);
            $course_questions = $this->question_model->read_question('course_id', $course_id);
            foreach ($course_videos as $key => $videos) {
            $videos->video_name = $this->video_model->read_video('id',$videos->video_id);
            
            
            }
        
            $data = [
                'view' => "./pages/add/add_course3.tpl",
                'videos' => $course_videos,
                'course_id' => $course_id,
                'questions' => $course_questions
        
            ];  
                
            $this->smarty->view("./admin/admin_layout.tpl", $data);
        }else{

        }
    }

    public function ajax(){
        $js_data = $_POST['data'];
        $js_video_id = $_POST['video_id'];
        $question_ids = array();
        $data = explode(",",$js_data);
        foreach ($data as $key => $value) {
            array_push($question_ids, $value);
        }
        foreach($question_ids as $key => $value) {
            $todb = [
                'video_id' => $js_video_id,
                'question_id' => $data[$key]
            ];
            $this->questionvideo_model->create_questionvideo($todb);
        }
    }

    public function course_added(){
        $courses = $this->course_model->read_course();
        $notification = [
            "type" => "success", 
            "label" => "Sikeres!",
            "message" => "Kurzus hozzáadva!",
            "dismissable" => TRUE
        ];
        $data = [
            'view' => "./pages/content/course.tpl",
            'notification' => $notification,
            'courses' => $courses
           ];
  
        $this->smarty->view("./admin/admin_layout.tpl", $data);
  
    }
    public function edit($id){
        echo "edit course";
    }

    public function delete(){
        echo "delete course";
    }

  
       
  


}
?>