<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends Public_Controller {
    private  $questions; 
    private $courses;
	public function __construct(){
        parent::__construct();
        $this->questions = $this->question_model->read_question();
        $this->courses = $this->course_model->read_course();

	}


    public function index(){
       
        $videos = $this->video_model->read_video();
              
        $data = [
            'view' => "./pages/content/video.tpl",
            'videos' => $videos,
            'courses' => $this->courses
        ];
     
        $this->smarty->view("./admin/admin_layout.tpl", $data);
        
    }



    public function ajax(){

        $js_data = $_POST['data'];
        $js_video_id = $_POST['video_id'];
        $question_ids = array();
        $data = explode(",",$js_data);
        foreach ($data as $key => $value) {
            array_push($question_ids, $value);
        }
        foreach($question_ids as $key => $value) {
            $todb = [
                'video_id' => $js_video_id,
                'question_id' => $data[$key]
            ];
            $this->questionvideo_model->create_questionvideo($todb);
        }
      
        

        // $uploaded = array();
        // if(!empty($_FILES['file']['name'][0])){
            
        //     foreach($_FILES['file']['name'] as $position => $name){
                
        //         if(move_uploaded_file($_FILES['file']['tmp_name'][$position], base_url().'assets/videos'.$name)){
        //                $uploaded[] = array(
        //                    'name' => $name,
        //                    'file' => 'uploads/'.$name
        //                );                                                                                                                           
        //         }       
        //     }   
        // }
        //   echo json_encode($uploaded); 
    }

    public function delete($id){
        // $videos = $this->video_model->read_video('id', $id)[0];
        // $videos->question = $this->blockquestion_model->read_blockquestion('video_id', $videos->id)[0];
        // $videos->answer = $this->blockanswer_model->read_blockanswer('question_id', $videos->question->id); 
        $video_url = $this->video_model->read_video('id', $id)[0]->url;
        if (file_exists($video_url)) {
            unlink($video_url);
        }
        $this->video_model->delete_video('id', $id);
        redirect('admin/videok');
     
    }

}
?>