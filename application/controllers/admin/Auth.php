<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth extends CI_Controller{

    public function __contruct()
    {
        parent::__construct();
        
    }	

    public function index(){

        if (!$this->session->has_userdata('logged_in')) {
            
            redirect('admin/login');
        }else{
            
            redirect('admin/kurzusok');
        }
        
    }
 
    public function register(){

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules("username","Name","required", array('required' => "Adjon meg felhasználónevet"));

        if ($this->form_validation->run() == FALSE) {
            
            $data['view'] = "./pages/login/register.tpl";
            $this->smarty->view("./admin/login_layout.tpl", $data);

            
        }else{

            $hashed = md5($this->input->post('password'));
            $data1 = array(

                'su_name' => $this->input->post('username'),
                'su_pass' => $hashed

            );
            $this->superuser_model->register_suser($data1);
            $data['error'] = '';
            $data['view'] = "./pages/login/login.tpl";
            $this->smarty->view("./admin/login_layout.tpl",$data);
           
        }
       
    }


    public function login(){
            if(!isset($_POST['submit'])){

                $data['view'] = "./pages/login/login.tpl";
                $this->smarty->view("./admin/login_layout.tpl",$data);

            }else{

                if (empty($_POST['username']) || empty($_POST['password'])) {

                        if (empty($_POST['username'])) {
                            $this->session->set_flashdata('username', "Mindenképen töltse ki.");
                        }
                        if(empty($_POST['password'])){
                           $this->session->set_flashdata('password', "Mindenképpen töltse ki.");
                        }
                        $data['username'] = $_POST['username'];
                        $data['password'] = $_POST['password'];
                        $data['view'] = "./pages/login/login.tpl";
                        $this->smarty->view("./admin/login_layout.tpl",$data);


                }else{

                    $user_id = $this->superuser_model->login_suser($_POST['username'], md5($_POST['password']));

                    if ($user_id != 0) {
                        $user_data = array(

                            'id' => $user_id,
                            'logged_in' => true
            
                          );
                        $this->session->set_userdata($user_data);
                        redirect("admin/kurzusok");
                    }else{
                        $this->session->set_flashdata('error', "Bejelentkezés meghiúsult!");
                        $data['username'] = $_POST['username'];
                        $data['password'] = $_POST['password'];
                        $data['view'] = "./pages/login/login.tpl";
                        $this->smarty->view("./admin/login_layout.tpl",$data);
                    }

                }

            }
    }

    public function password_change(){




        if (!isset($_POST['submit'])) {
            $data['view'] = "./pages/login/password_change.tpl";
            $this->smarty->view('./admin/admin_layout.tpl',$data);
        }else{

            if (empty($_POST['newPassword1']) || empty($_POST['newPassword2']) || empty($_POST['oldPassword'])) {
             
               $this->session->set_flashdata('error', "Minden mezőt töltsön ki!");

               $data['view'] = "./pages/login/password_change.tpl";
               $this->smarty->view("./admin/admin_layout.tpl",$data);

            }else{
                $message = $_POST['newPassword1'] != $_POST['newPassword2'] ? "Hibás új jelszó" : "0";

                if ($message != 0) {
                    $this->session->set_flashdata('error', $message);
                    
                    $data['view'] = "./pages/login/password_change.tpl";
                    $this->smarty->view("./admin/admin_layout.tpl",$data);
                }else{
                    $hashedOldPass = md5($_POST['oldPassword']);
                    $hashedNewPass = md5($_POST['newPassword1']);
                    $message1 = ($this->superuser_model->password_change_su($hashedOldPass, $hashedNewPass) === '0' ? 'Hibás régi jelszó!' : '0');
                    if ($message1 != '0') {

                        $this->session->set_flashdata('error', $message1);

                        $data['view'] = "./pages/login/password_change.tpl";
                        $this->smarty->view("./admin/admin_layout.tpl",$data); 

                    }else{

                        $this->logout();

                    }
                }
            }
        }
    
    }

    public function logout(){
        //unset session data
        session_destroy();
        redirect('admin');
    }

}

?>