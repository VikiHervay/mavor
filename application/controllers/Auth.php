<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth extends CI_Controller{

    public function __contruct()
    {
        parent::__construct();
        
    }	

    public function index(){

        if (!$this->session->has_userdata('purchase_id')) {
            
            redirect('belepes');
        }else{
            
            redirect('home/content');
        }
        
    }
 
    public function register(){

       
       
    }


    public function login(){

        if (!isset($_POST['submit'])) {
            $data = [
                'view' => "./pages/login/login.tpl"
               ];
               $this->smarty->view("./front/front_login_layout.tpl", $data);
        }else{
            if (empty($_POST['id'])) {
                $this->session->set_flashdata('id', "Mindenképen töltse ki.");
                $data = [
                    'view' => "./pages/login/login.tpl"
                   ];
                   $this->smarty->view("./front/front_login_layout.tpl", $data);
            
            }else{
                redirect('home/content');
            }
        }
  
    }

    public function password_change(){




        if (!isset($_POST['submit'])) {
            $data['view'] = "./pages/login/password_change.tpl";
            $this->smarty->view('./admin/admin_layout.tpl',$data);
        }else{

            if (empty($_POST['newPassword1']) || empty($_POST['newPassword2']) || empty($_POST['oldPassword'])) {
             
               $this->session->set_flashdata('error', "Minden mezőt töltsön ki!");

               $data['view'] = "./pages/login/password_change.tpl";
               $this->smarty->view("./admin/admin_layout.tpl",$data);

            }else{
                $message = $_POST['newPassword1'] != $_POST['newPassword2'] ? "Hibás új jelszó" : "0";

                if ($message != 0) {
                    $this->session->set_flashdata('error', $message);
                    
                    $data['view'] = "./pages/login/password_change.tpl";
                    $this->smarty->view("./admin/admin_layout.tpl",$data);
                }else{
                    $hashedOldPass = md5($_POST['oldPassword']);
                    $hashedNewPass = md5($_POST['newPassword1']);
                    $message1 = ($this->superuser_model->password_change_su($hashedOldPass, $hashedNewPass) === '0' ? 'Hibás régi jelszó!' : '0');
                    if ($message1 != '0') {

                        $this->session->set_flashdata('error', $message1);

                        $data['view'] = "./pages/login/password_change.tpl";
                        $this->smarty->view("./admin/admin_layout.tpl",$data); 

                    }else{

                        $this->logout();

                    }
                }
            }
        }
    
    }

    public function logout(){
        //unset session data
        session_destroy();
        redirect('admin');
    }

}

?>