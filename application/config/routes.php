<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';


// setting route for admin
$route['admin'] = 'admin/auth';
$route['admin/login'] = 'admin/auth/login';

// $route['admin/login'] = 'admin/auth/register';

$route['admin/videok'] = 'admin/videos';
$route['admin/videok_hozzaad'] = 'admin/videos/add';
$route['admin/videok_szerkeszt'] = 'admin/videos/edit';
$route['admin/kerdesek'] = 'admin/questions';
$route['admin/kerdesek_hozzaad'] = 'admin/questions/add';
$route['admin/kerdesek_szerkeszt'] = 'admin/questions/edit';
$route['admin/kurzusok'] = 'admin/courses';
$route['admin/kurzusok_hozzaad'] = 'admin/courses/add';
$route['admin/kurzusok_hozzaad_2/(:any)'] = 'admin/courses/add_step_2/$1';
$route['admin/kurzusok_hozzaad_3/(:any)'] = 'admin/courses/add_step_3/$1';
$route['admin/kurzusok_szerkeszt'] = 'admin/courses/edit';
// $route['admin/projektek/szerkeszt/(:any)'] = 'admin/project/edit/$1';

$route['belepes'] = 'auth/login';
$route['kurzus'] = 'home/content';
$route['kategoriak'] = 'categories';
$route['kategoriak/(:any)'] = 'categories/index/$1';
$route['vasarlas'] = 'buying';
$route['vasarlas/(:any)'] = 'buying/index/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
