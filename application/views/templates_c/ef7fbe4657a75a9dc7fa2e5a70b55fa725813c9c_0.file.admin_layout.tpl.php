<?php
/* Smarty version 3.1.33, created on 2020-02-21 22:16:32
  from 'D:\MyPrograms\xammp\htdocs\szatmarvill\application\views\templates\admin\admin_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e5048b07653c0_84797351',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef7fbe4657a75a9dc7fa2e5a70b55fa725813c9c' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\admin_layout.tpl',
      1 => 1582319789,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./template/head.tpl' => 1,
    'file:./template/header.tpl' => 1,
    'file:./template/footer.tpl' => 1,
  ),
),false)) {
function content_5e5048b07653c0_84797351 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $_smarty_tpl->_subTemplateRender("file:./template/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div id="page_wrapper" class="d-flex flex-column justify-content-between">
        <div id="header_wrapper">
			<?php $_smarty_tpl->_subTemplateRender("file:./template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </div>

        <div id="content_wrapper" class="container">
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['view']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>
	</div>
	<?php $_smarty_tpl->_subTemplateRender("file:./template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
</html><?php }
}
