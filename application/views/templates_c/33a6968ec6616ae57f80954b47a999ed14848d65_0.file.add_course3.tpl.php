<?php
/* Smarty version 3.1.33, created on 2020-03-31 06:16:17
  from 'D:\MyPrograms\xammp\htdocs\mavor\application\views\templates\admin\pages\add\add_course3.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e82c4118f7b41_98377078',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '33a6968ec6616ae57f80954b47a999ed14848d65' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\add\\add_course3.tpl',
      1 => 1585628142,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e82c4118f7b41_98377078 (Smarty_Internal_Template $_smarty_tpl) {
?><!--<form action="<?php echo base_url();?>
admin/kurzusok_hozzaad_3/<?php echo $_smarty_tpl->tpl_vars['course_id']->value;?>
" method="POST">-->
         <div class="row flex-column align-items-center justify-content-center m-4">
             <div class="col text-center">
                <h4>Kurzus feltöltése 3/3.</h4>
            </div>
            <div class="col text-center">
                <p>Válassza ki melyik videóhoz melyik kérdés tartozik és kattintson az OK gombra</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-md-6">
                 <div class="my-card border p-5">
                   <div class="row justify-content-center">
                          <h3>Videók</h3>
                    </div>
                     <div class="row justify-content-center">
                           <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['videos']->value, 'video', false, 'video_key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['video_key']->value => $_smarty_tpl->tpl_vars['video']->value) {
?>
                                <div class="col-6">
                                     <p class="list-group-item line-break picked-video" data-value="<?php echo $_smarty_tpl->tpl_vars['video']->value->video_id;?>
"><?php echo $_smarty_tpl->tpl_vars['video']->value->video_name[0]->video_name;?>
</p>
                                </div>
                                <div class="col-6 d-flex align-items-center justify-content-center">
                                 <input type="checkbox" name="video_id" class="list-group-item form-check-input picked-video-id"  value="<?php echo $_smarty_tpl->tpl_vars['video']->value->video_id;?>
">
                                </div>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            </div>
             <div class="col col-md-6">
                 <div class="my-card border p-5">
                   <div class="row justify-content-center">
                          <h3>Kérdések</h3>
                    </div>

                    <div class="row justify-content-center">
                           <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questions']->value, 'question', false, 'q_key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['q_key']->value => $_smarty_tpl->tpl_vars['question']->value) {
?>
                                <div class="col-6">
                                     <p class="list-group-item line-break picked-question" data-value="<?php echo $_smarty_tpl->tpl_vars['question']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['question']->value->question;?>
</p>
                                </div>
                                <div class="col-6 d-flex align-items-center justify-content-center">
                                 <input type="checkbox" name="question_id[]" class="list-group-item form-check-input picked-question-id"  value="<?php echo $_smarty_tpl->tpl_vars['question']->value->id;?>
">
                                </div>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                  
                </div>
            </div>
        </div>
        <div class="row justify-content-center m-3">
            <button id="match" class="btn btn-success" type="submit" name="match_questions">OK</button>
        </div>
        

<!--</form>--><?php }
}
