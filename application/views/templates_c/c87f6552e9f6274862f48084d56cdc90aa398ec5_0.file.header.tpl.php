<?php
/* Smarty version 3.1.33, created on 2020-03-25 11:01:10
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\template\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e7b2be67cd392_60679381',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c87f6552e9f6274862f48084d56cdc90aa398ec5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\template\\header.tpl',
      1 => 1585128614,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e7b2be67cd392_60679381 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?php echo base_url();?>
admin">MAVOR</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/videok">Videók</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/videok_hozzaad">Videók hozzáadása</a>
      </li>
     <!-- <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/kerdesek">Kurzus kérdéssor</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/kerdesek_hozzaad">Kérdéssor hozzáadása</a>
      </li>-->
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/kurzusok">Kurzusok</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/kurzusok_hozzaad">Kurzusok hozzáadása</a>
      </li>
    </ul>  
      
    <a class="btn btn-outline-warning my-2 my-sm-0 mx-2" type="submit" href="<?php echo base_url();?>
admin/auth/password_change">Jelszó megváltoztatása</a>

      <a class="btn btn-outline-danger my-2 my-sm-0" type="submit" href="<?php echo base_url();?>
admin/auth/logout">Kijelentkezés</a>
       
  </div>

</nav><?php }
}
