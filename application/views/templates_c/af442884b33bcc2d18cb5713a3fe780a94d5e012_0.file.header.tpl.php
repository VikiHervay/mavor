<?php
/* Smarty version 3.1.33, created on 2020-02-21 18:23:03
  from 'D:\MyPrograms\xammp\htdocs\szatmarvill\application\views\templates\admin\template\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e5011f70772b6_47675083',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'af442884b33bcc2d18cb5713a3fe780a94d5e012' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\template\\header.tpl',
      1 => 1582305778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e5011f70772b6_47675083 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?php echo base_url();?>
admin">Szatmárvill Kft.</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/projektek">Projektek</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/projektek_hozzaad">Projekt hozzáadása</a>
      </li>
    </ul>  
      
    <a class="btn btn-outline-warning my-2 my-sm-0 mx-2" type="submit" href="<?php echo base_url();?>
admin/auth/password_change">Jelszó megváltoztatása</a>

      <a class="btn btn-outline-danger my-2 my-sm-0" type="submit" href="<?php echo base_url();?>
admin/auth/logout">Kijelentkezés</a>
       
  </div>

</nav><?php }
}
