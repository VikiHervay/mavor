<?php
/* Smarty version 3.1.33, created on 2020-02-21 11:28:09
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\front\_partials\header_nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4fb0b960a062_95038261',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3d433349bd3ded9eb5021b691bc2f7ab6fdb9411' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\front\\_partials\\header_nav.tpl',
      1 => 1582280886,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4fb0b960a062_95038261 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#">
            <img src="<?php echo base_url();?>
assets/images/szatmarvill-logo.svg" alt="Szatmárvill logo" class="d-inline-block align-top" width="160">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fa fa-home"></i>
            </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="#about_ut">Bemutatkozás <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#projects">Projektek</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#services">Tevékenységeink</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact_now">Kapcsolat</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-warning btn-quote" href="#offer">
                    Kérjen ajánlatot
                </a>
            </li>
            </ul>
        </div>
    </nav>
</div><?php }
}
