<?php
/* Smarty version 3.1.33, created on 2020-03-25 11:01:10
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\pages\content\video.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e7b2be68e76b8_81135830',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '44cc8002d38a997295b051eb0b86f1baa2cb0c72' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\content\\video.tpl',
      1 => 1585128614,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e7b2be68e76b8_81135830 (Smarty_Internal_Template $_smarty_tpl) {
?><table class="table">
	<thead>
	<tr>
		<th  scope="col">Videók</th>
		<th class='text-center' scope="col">Törlés</th>
	</tr>
	</thead>
	<tbody>


	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['videos']->value, 'video', false, 'index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['index']->value => $_smarty_tpl->tpl_vars['video']->value) {
?>
	<tr> 
		<td><?php echo $_smarty_tpl->tpl_vars['video']->value->video_name;?>
</td>      
		
		<td class='text-center'><a class='btn btn-danger' type='button' href="<?php echo base_url();?>
admin/videos/delete/<?php echo $_smarty_tpl->tpl_vars['video']->value->id;?>
"><i class='fas fa-trash'></i></a></td>
			</tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

	</tbody>
</table>


<?php }
}
