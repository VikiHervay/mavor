<?php
/* Smarty version 3.1.33, created on 2020-02-18 08:13:01
  from 'C:\xampp\htdocs\keletagora\application\views\templates\front\_partials\footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4b8e7dbdb013_09948077',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '12276c0d865b576461acd2dd118e788ea9de0571' => 
    array (
      0 => 'C:\\xampp\\htdocs\\keletagora\\application\\views\\templates\\front\\_partials\\footer.tpl',
      1 => 1582009567,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4b8e7dbdb013_09948077 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Footer -->
  <footer class="py-5">
  
    <div class="container">

      
      <div class="small text-center" style="color: #f4623a" >
      <span style="float: left;">
        <a href="https://www.ad4est.com/" target="blank">Copyright &copy;  AD4EST Kft. - 2020</a>
      </span>
      <span style="float: right;">
        <a href="<?php echo base_url();?>
assets/Kelet_agora_aszf.pdf" target="blank">ÁSZF</a>
      </span>
      <span class="pr-2" style="float: right;">
        <a href="<?php echo base_url();?>
assets/Adatkezelesi_tajekoztato_Kelet-Agora_kft.pdf" target="blank">GDPR</a>
      </span>
      </div>
    </div>
  </footer> 

   <?php }
}
