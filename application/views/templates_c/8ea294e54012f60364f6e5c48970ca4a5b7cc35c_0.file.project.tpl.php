<?php
/* Smarty version 3.1.33, created on 2020-02-21 22:15:51
  from 'D:\MyPrograms\xammp\htdocs\szatmarvill\application\views\templates\admin\pages\project\project.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e5048878af4e3_39755800',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8ea294e54012f60364f6e5c48970ca4a5b7cc35c' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\project\\project.tpl',
      1 => 1582319747,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e5048878af4e3_39755800 (Smarty_Internal_Template $_smarty_tpl) {
?><table class="table">
	<thead>
	<tr>
		<th  scope="col">Projektek</th>
		<th class='text-center' scope="col">Törlés</th>
		<th class='text-center' scope="col">Szerkesztés</th>
	</tr>
	</thead>
	<tbody>


	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['projects']->value, 'project', false, 'index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['index']->value => $_smarty_tpl->tpl_vars['project']->value) {
?>
	<tr> 
		<td><?php echo $_smarty_tpl->tpl_vars['project']->value->name;?>
</td>      
		
		<td class='text-center'><a class='btn btn-danger' type='button' href="<?php echo base_url();?>
admin/project/delete/<?php echo $_smarty_tpl->tpl_vars['project']->value->id;?>
"><i class='fas fa-trash'></i></a></td>
		<td class='text-center'><a class='btn btn-warning' role='button' href='<?php echo base_url();?>
admin/project/edit/<?php echo $_smarty_tpl->tpl_vars['project']->value->url;?>
'><i class='far fa-edit'></i></a></td>		 
	</tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

	</tbody>
</table>


<?php }
}
