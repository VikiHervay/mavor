<?php
/* Smarty version 3.1.33, created on 2020-03-31 09:07:59
  from 'C:\xampp\htdocs\mavor\application\views\templates\front\_partials\sidebar.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e82ec4f238705_85988447',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '655e9f39cad5d8eaf842e7ac5576833a79933115' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\front\\_partials\\sidebar.tpl',
      1 => 1585637755,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e82ec4f238705_85988447 (Smarty_Internal_Template $_smarty_tpl) {
?><nav id="sidebar">
        <div class="sidebar-header d-flex justify-content-center">
            <h3>MAVOR</h3>
        </div>

        <ul class="list-unstyled components">
          
            <li class="list-group-item">
                <video class="side-video" muted preload="true" width="180"  height="135"  volume="0">
                    <source src="<?php echo base_url();?>
assets/videos/kurzus_1.mp4" type="video/mp4">
                </video>
            </li>
            <li class="list-group-item">
                <video class="side-video" muted preload="true" width="180"  height="135"  volume="0">
                    <source src="<?php echo base_url();?>
assets/videos/kurzus_2.mp4" type="video/mp4">
                </video>
            </li>
            <li class="list-group-item">
                <video class="side-video" muted preload="true" width="180"  height="135"  volume="0">
                    <source src="<?php echo base_url();?>
assets/videos/kuruzs_3.mp4" type="video/mp4">
                </video>
            </li>
            
        </ul>
    </nav>
<?php }
}
