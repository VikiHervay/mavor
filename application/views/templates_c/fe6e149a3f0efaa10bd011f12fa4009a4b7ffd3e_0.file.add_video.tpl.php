<?php
/* Smarty version 3.1.33, created on 2020-03-29 08:32:32
  from 'D:\MyPrograms\xammp\htdocs\mavor\application\views\templates\admin\pages\add\add_video.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e804100c28784_75296540',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fe6e149a3f0efaa10bd011f12fa4009a4b7ffd3e' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\add\\add_video.tpl',
      1 => 1585463478,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e804100c28784_75296540 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="" method="POST" enctype="multipart/form-data">

<div class="row justify-content-center px-1 py-5">
    <?php if (isset($_SESSION['error'])) {?>
        <div class="col-6">
            <div class="form-group alert alert-danger text-center alert-dismissible fade show" role="alert">
                    <?php echo $_SESSION['error'];?>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
            </div>
        </div>
    <?php }?>
    <?php if (isset($_SESSION['success'])) {?>
        <div class="col-3">
            <div class="form-group alert alert-success alert-dismissible fade show" role="alert">
                <?php echo $_SESSION['success'];?>

                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    <?php }?>
</div>
    <div class="row justify-content-center">

        <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                    <label> Videó címe: </label>
                    <textarea name="name" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['name']->value)) {
echo $_smarty_tpl->tpl_vars['name']->value;
}?></textarea>
            </div>    
        </div>
         <div class="col col-md-6 text-center form-group">

                <div id="uploads"></div>
                <div class="dropzone" id="dropzone">
                    Drop files fere to upload
                </div>

                    </div>
      
       
    </div>
    
    <div class="row  justify-content-center px-1">
         <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                <label>Videóhoz tartozó kérdés kiválasztása: </label><br>
                <ul id="question-list">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['questions']->value, 'question', false, 'index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['index']->value => $_smarty_tpl->tpl_vars['question']->value) {
?>
                       <li><a class="dropdown-question" data-value="<?php echo $_smarty_tpl->tpl_vars['question']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['question']->value->question;?>
</a></li>  
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 
                </ul>
            </div>
        </div>
        <input type="hidden" name="question_id" id="question_id" value="0"/>

         <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                <label>Melyik kurzushoz tartozik a videó: </label><br>
                <ul id="question-list">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['courses']->value, 'course', false, 'index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['index']->value => $_smarty_tpl->tpl_vars['course']->value) {
?>
                       <li><a class="dropdown-course" data-value="<?php echo $_smarty_tpl->tpl_vars['course']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['course']->value->course_name;?>
</a></li>  
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 
                </ul>
            </div>
        </div>
        <input type="hidden" name="course_id" id="course_id" value="0"/>
        
    </div>


    <div class="row justify-content-center p-5">
        <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-success btn-lg" name="submit" value="Mentés" /></div>
    </div>
</div>

</form>

<?php }
}
