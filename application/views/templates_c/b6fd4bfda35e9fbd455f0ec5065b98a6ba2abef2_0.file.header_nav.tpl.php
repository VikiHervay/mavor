<?php
/* Smarty version 3.1.33, created on 2020-03-23 11:39:43
  from 'D:\MyPrograms\xammp\htdocs\mavor\application\views\templates\front\_partials\header_nav.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e7891efbf49a8_09285261',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b6fd4bfda35e9fbd455f0ec5065b98a6ba2abef2' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\mavor\\application\\views\\templates\\front\\_partials\\header_nav.tpl',
      1 => 1582294968,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e7891efbf49a8_09285261 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#">
            <img src="<?php echo base_url();?>
assets/images/szatmarvill-logo.svg" alt="Szatmárvill logo" class="d-inline-block align-top" width="160">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fa fa-home"></i>
            </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="#about_ut">Bemutatkozás <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#projects">Projektek</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#services">Tevékenységeink</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact_now">Kapcsolat</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-warning btn-quote" href="#offer">
                    Kérjen ajánlatot
                </a>
            </li>
            </ul>
        </div>
    </nav>
</div><?php }
}
