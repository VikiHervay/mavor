<?php
/* Smarty version 3.1.33, created on 2020-03-31 09:07:56
  from 'C:\xampp\htdocs\mavor\application\views\templates\front\pages\login\login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e82ec4c867168_59811633',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5a3f2cb84c2512f3a2407a56b2ccc0e245261ae0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\front\\pages\\login\\login.tpl',
      1 => 1585637755,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e82ec4c867168_59811633 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand">MAVOR</a>
</nav>

<?php if (isset($_SESSION['error'])) {?>
<div class="row py-3 justify-content-center">
    <div class="col-3">
        <div class="form-group alert alert-danger text-center" role="alert" >
            <?php echo $_SESSION['error'];?>

        </div>
    </div>
</div>

<?php }?>

<div class="row py-5 justify-content-center">
    <div class="col-3">
    
            <div class="form-group">
                <h3>Belépés</h3>
            </div>
        
            <form action="" method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Kód:</label>
                    <input type="text" name="id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Felhasználónév" value="<?php if ($_smarty_tpl->tpl_vars['username']->value) {
echo $_smarty_tpl->tpl_vars['username']->value;
}?>">
                </div>
                <?php if (isset($_SESSION['id'])) {?>
                <div class="form-group alert alert-danger text-center"  role="alert">
                        <?php echo $_SESSION['id'];?>

                </div>
                <?php }?>

                <div class="form-group d-flex justify-content-center">
                    <button type="submit" name="submit" class="btn btn-primary">Belépés</button>
                </div>
            </form>
    </div>
</div><?php }
}
