<?php
/* Smarty version 3.1.33, created on 2020-03-25 11:01:04
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\login_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e7b2be0584011_42713026',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'de4f10ff14b30bb89ff85056447cc24b37b7a455' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\login_layout.tpl',
      1 => 1585128614,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./template/head.tpl' => 1,
    'file:./template/footer.tpl' => 1,
  ),
),false)) {
function content_5e7b2be0584011_42713026 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $_smarty_tpl->_subTemplateRender("file:./template/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div id="page_wrapper" class="d-flex flex-column justify-content-between">
  
        <div id="content_wrapper" class="">
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['view']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>

        <div id="footer_wrapper" class="pt-5 text-white">
			<?php $_smarty_tpl->_subTemplateRender("file:./template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </div>
	</div>
</body>
</html>
<?php }
}
