<?php
/* Smarty version 3.1.33, created on 2020-03-31 09:07:56
  from 'C:\xampp\htdocs\mavor\application\views\templates\front\front_login_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e82ec4c5d8128_74923130',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e66abb7a1c0e9dfe82efda9165e797b7f01ac66f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\front\\front_login_layout.tpl',
      1 => 1585637755,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./_partials/head.tpl' => 1,
  ),
),false)) {
function content_5e82ec4c5d8128_74923130 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $_smarty_tpl->_subTemplateRender("file:./_partials/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div id="page_wrapper" class="d-flex flex-column justify-content-between">
  
        <div id="content_wrapper" class="">
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['view']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>

        <div id="footer_wrapper" class="pt-5 text-white">
			
        </div>
	</div>
</body>
</html>
<?php }
}
