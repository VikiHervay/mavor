<?php
/* Smarty version 3.1.33, created on 2020-03-25 13:54:02
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\pages\add\add_course.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e7b546a79a3e3_92065922',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dd50eb48a9eee0a2f63f2ffd628d2431123860ac' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\add\\add_course.tpl',
      1 => 1585140699,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e7b546a79a3e3_92065922 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="" method="POST" enctype="multipart/form-data">
    <div class="row justify-content-center px-1 py-5">
        <?php if (isset($_SESSION['error'])) {?>
            <div class="col-6">
                <div class="form-group alert alert-danger text-center alert-dismissible fade show" role="alert">
                        <?php echo $_SESSION['error'];?>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
            </div>
        <?php }?>
        <?php if (isset($_SESSION['success'])) {?>
            <div class="col-3">
                <div class="form-group alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo $_SESSION['success'];?>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        <?php }?>
    </div>

    <div class="row  justify-content-center px-1">
            <div class="col col-md-6">Kurzus címe:</div>
            <div class="col col-md-6 text-center form-group"><textarea name="name" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['name']->value)) {
echo $_smarty_tpl->tpl_vars['name']->value;
}?></textarea></div>
    </div>
     <div class="row  justify-content-center px-1">
            <div class="col col-md-6">Kredit értéke:</div>
            <div class="col col-md-6 text-center form-group"><textarea name="credit" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['credit']->value)) {
echo $_smarty_tpl->tpl_vars['credit']->value;
}?></textarea></div>
    </div>



    <div class="row  justify-content-center px-1">
            <div class="col col-md-6 ">Kurzuskérdések feltöltése:</div>
            <div class="col col-md-6 text-center form-group"><input type="file" name="userfile" size="20" multiple="multiple" /></div>
    </div>

	<div class="row justify-content-center p-5">
        <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-primary" name="submit" value="Mentés" /></div>
	</div>

</form><?php }
}
