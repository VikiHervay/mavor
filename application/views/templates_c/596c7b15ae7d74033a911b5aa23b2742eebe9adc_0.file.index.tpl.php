<?php
/* Smarty version 3.1.33, created on 2020-02-20 11:36:53
  from 'C:\xampp\htdocs\keletagora\application\views\templates\front\pages\categories\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4e61456406a7_85714498',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '596c7b15ae7d74033a911b5aa23b2742eebe9adc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\keletagora\\application\\views\\templates\\front\\pages\\categories\\index.tpl',
      1 => 1582194975,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4e61456406a7_85714498 (Smarty_Internal_Template $_smarty_tpl) {
?>  <section class="page-section bg-light" id="lampak">
    <div class="container">
      <div class="row">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lamps']->value, 'lamp', false, 'lamp_index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['lamp_index']->value => $_smarty_tpl->tpl_vars['lamp']->value) {
?>
        <?php if ($_smarty_tpl->tpl_vars['lamp_index']->value%2 != 0 && $_smarty_tpl->tpl_vars['lamp_index']->value%3 == 0) {?>
            </div>
            <div class="row justify-content-center">	
                    <div class="col-12 col-md-4 mt-3 mb-3">
                        <div class="card profile-card-5 h-100">
                            <div class="card-img-block h-100">
                                <img class="card-img-top " src="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['lamp']->value->image;?>
" alt="Card image cap">
                            </div>
                            <div class="card-body pt-0 text-center h-100">
                                <h5 class="d-block card-title"> <?php echo $_smarty_tpl->tpl_vars['lamp']->value->lamp_name;?>
 </h5>
                            </div>
                            <div class="card-body pt-0 h-100">
                                <span class="item-number">
                                    <h5 class="d-block small" >Cikkszám: <?php echo $_smarty_tpl->tpl_vars['lamp']->value->item_number;?>
 </h5>
                                </span>
                                <span class="price">
                                    <h5 class="pull-right d-block">Ár: <?php echo $_smarty_tpl->tpl_vars['lamp']->value->price;?>
 Ft </h5>
                                </span>
                            </div>
                            <div class="card-body pt-0 text-center h-100">
                                <a class="d-block btn buy-btn-move w-100" type="submit" href="<?php echo base_url();?>
vasarlas/<?php echo $_smarty_tpl->tpl_vars['lamp']->value->id;?>
/#form">Megveszem</a>
                            <!-- <form method="POST" action="<?php echo base_url();?>
buying/#form"><input type="hidden" name="lamp_id" value="<?php echo $_smarty_tpl->tpl_vars['lamp']->value->id;?>
"/><input class="d-block btn buy-btn-move w-100" type="submit" value='Megveszem' /></form>-->
                            </div>
                            
                        </div>
                    
            </div>

            <?php } else { ?>
                <div class="col-12 col-md-4 mt-3 mb-3">
                        <div class="card profile-card-5 h-100">
                            <div class="card-img-block h-100">
                                <img class="card-img-top " src="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['lamp']->value->image;?>
" alt="Card image cap">
                            </div>
                            <div class="card-body pt-0 text-center h-100">
                                <h5 class="d-block card-title"> <?php echo $_smarty_tpl->tpl_vars['lamp']->value->lamp_name;?>
 </h5>
                            </div>
                            <div class="card-body pt-0 h-100">
                                <span class="item-number">
                                    <h5 class="d-block small" >Cikkszám: <?php echo $_smarty_tpl->tpl_vars['lamp']->value->item_number;?>
 </h5>
                                </span>
                                <span class="price">
                                    <h5 class="pull-right d-block">Ár: <?php echo $_smarty_tpl->tpl_vars['lamp']->value->price;?>
 Ft </h5>
                                </span>
                            </div>
                            <div class="card-body pt-0 text-center h-100">
                                <a class="d-block btn buy-btn-move w-100" type="submit" href="<?php echo base_url();?>
vasarlas/<?php echo $_smarty_tpl->tpl_vars['lamp']->value->id;?>
/#form">Megveszem</a>
                            <!-- <form method="POST" action="<?php echo base_url();?>
buying/#form"><input type="hidden" name="lamp_id" value="<?php echo $_smarty_tpl->tpl_vars['lamp']->value->id;?>
"/><input class="d-block btn buy-btn-move w-100" type="submit" value='Megveszem' /></form>-->
                            </div>
                            
                        </div>
                </div>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        
    </div>
  
  </section><?php }
}
