<?php
/* Smarty version 3.1.33, created on 2020-03-23 11:35:05
  from 'D:\MyPrograms\xammp\htdocs\mavor\application\views\templates\admin\login_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e7890d9696d07_27566873',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3c7556db9b96116ab6fa565b4377914437d7ad58' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\mavor\\application\\views\\templates\\admin\\login_layout.tpl',
      1 => 1582294968,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./template/head.tpl' => 1,
    'file:./template/footer.tpl' => 1,
  ),
),false)) {
function content_5e7890d9696d07_27566873 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $_smarty_tpl->_subTemplateRender("file:./template/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div id="page_wrapper" class="d-flex flex-column justify-content-between">
  
        <div id="content_wrapper" class="">
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['view']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>

        <div id="footer_wrapper" class="pt-5 text-white">
			<?php $_smarty_tpl->_subTemplateRender("file:./template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </div>
	</div>
</body>
</html>
<?php }
}
