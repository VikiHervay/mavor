<?php
/* Smarty version 3.1.33, created on 2020-03-25 16:05:13
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\pages\add\add_video.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e7b7329acdc92_99287117',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '92078be5c04f6264a4411fc9cda7ef7b2d6bfd3b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\add\\add_video.tpl',
      1 => 1585148710,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e7b7329acdc92_99287117 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="" method="POST" enctype="multipart/form-data">
<div class="row justify-content-center px-1 py-5">
    <?php if (isset($_SESSION['error'])) {?>
        <div class="col-6">
            <div class="form-group alert alert-danger text-center alert-dismissible fade show" role="alert">
                    <?php echo $_SESSION['error'];?>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
            </div>
        </div>
    <?php }?>
    <?php if (isset($_SESSION['success'])) {?>
        <div class="col-3">
            <div class="form-group alert alert-success alert-dismissible fade show" role="alert">
                <?php echo $_SESSION['success'];?>

                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    <?php }?>
</div>

    <div class="row  justify-content-center px-1">
        <div class="col col-md-6">Videó címe:</div>
        <div class="col col-md-6 text-center form-group"><textarea name="name" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['name']->value)) {
echo $_smarty_tpl->tpl_vars['name']->value;
}?></textarea></div>
    </div>

    <div class="row  justify-content-center px-1">
        <div class="col col-md-6 ">Videó feltöltése:</div>
        <div class="col col-md-6 text-center form-group"><input type="file" name="userfile" size="20" multiple="multiple"/></div>
    </div>

    <div class="row justify-content-center">
        <div class="btn-group">
            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Videó kiválasztása
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Separated link</a>
            </div>
        </div>
    </div>


    <div class="row justify-content-center p-5">
        <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-primary" name="submit" value="Mentés" /></div>
    </div>


</form>
<?php }
}
