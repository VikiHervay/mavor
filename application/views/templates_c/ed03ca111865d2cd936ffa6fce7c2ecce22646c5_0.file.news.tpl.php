<?php
/* Smarty version 3.1.33, created on 2020-02-21 12:40:42
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\admin\pages\news\news.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4fc1ba17bd44_97673299',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ed03ca111865d2cd936ffa6fce7c2ecce22646c5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\news\\news.tpl',
      1 => 1582285240,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4fc1ba17bd44_97673299 (Smarty_Internal_Template $_smarty_tpl) {
?><table class="table">
	<thead>
	<tr>
		<th  scope="col">Hírek</th>
		<th class='text-center' scope="col">Törlés</th>
		<th class='text-center' scope="col">Szerkesztés</th>
		<th class='text-center' scope="col">Megosztás</th>
	</tr>
	</thead>
	<tbody>


	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['news']->value, 'new', false, 'index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['index']->value => $_smarty_tpl->tpl_vars['new']->value) {
?>
	<tr> 
		<td><?php echo $_smarty_tpl->tpl_vars['new']->value->news_title;?>
</td>      
		
		<td class='text-center'><form action="admin/news/delete/<?php echo $_smarty_tpl->tpl_vars['new']->value->id;?>
" method="POST"><button class='btn btn-danger' type='submit' name='submit'><i class='fas fa-trash'></i></button></td></form>
		<td class='text-center'><a class='btn btn-warning' role='button' href='<?php echo base_url();?>
admin/news/edit/<?php echo $_smarty_tpl->tpl_vars['new']->value->url;?>
'><i class='far fa-edit'></i></a></td>		 
	</tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

	</tbody>
</table>


<?php }
}
