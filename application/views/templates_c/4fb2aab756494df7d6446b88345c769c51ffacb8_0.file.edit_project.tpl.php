<?php
/* Smarty version 3.1.33, created on 2020-02-21 22:00:57
  from 'D:\MyPrograms\xammp\htdocs\szatmarvill\application\views\templates\admin\pages\edit_project\edit_project.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e504509109ba6_45148028',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4fb2aab756494df7d6446b88345c769c51ffacb8' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\edit_project\\edit_project.tpl',
      1 => 1582318845,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e504509109ba6_45148028 (Smarty_Internal_Template $_smarty_tpl) {
?>	<?php if (isset($_SESSION['error'])) {?>
	<div class="form-group alert alert-danger text-center" role="alert">
		<?php echo $_SESSION['error'];?>

	</div>
	<?php }?>
	 <?php if (isset($_SESSION['img_is_toobig'])) {?>
        <div class="col-3">
            <div class="form-group alert alert-danger" role="alert">
                <?php echo $_SESSION['img_is_toobig'];?>

            </div>
        </div>
    <?php }?>
<div class="row border py-2 justify-content-center">
	<form action="" method="POST" enctype="multipart/form-data">
			
		<div class="row justify-content-center px-1">
			<div class="col col-md-6 text-center font-weight-bold">Cím:</div>
			<div class="col col-md-6 text-center form-group"><textarea name="project_name" class="form-control"><?php echo $_smarty_tpl->tpl_vars['project']->value->name;?>
</textarea></div>
		</div>

		<div class="row justify-content-center px-1">
			<div class="col col-md-6 text-center font-weight-bold">Leírás:</div>
			<div class="col col-md-6 text-center form-group"><textarea rows="10" name="description" class="form-control"><?php echo $_smarty_tpl->tpl_vars['project']->value->description;?>
</textarea></div>
		</div>


		<div class="row justify-content-center px-1">
			<div class="col col-md-6 text-center font-weight-bold ">Képek hozzáadása:</div>
			<div class="col col-md-6 text-center form-group"><input type="file" name="userfile[]" size="20" multiple="multiple" /></div>
		</div>

	<div class="border" >
		<div class='row justify-content-center align-items-center'>
			<div class="col-4 text-center font-weight-bold">
				Feltöltött képek
			</div>
			<div class="col-4 text-center font-weight-bold">
				Kép kiváltasztása törlésre
			</div>
		</div>

		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['images']->value, 'image', false, 'index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['index']->value => $_smarty_tpl->tpl_vars['image']->value) {
?>

			<div class='row justify-content-center align-items-center'>
				<div class='border-right col-4 py-1 d-flex justify-content-center'>					 
						<img src='<?php echo base_url();
echo $_smarty_tpl->tpl_vars['image']->value->src;?>
' width='30%' />
					
				</div>
				<div class='col-4 text-center'>
					<input type="checkbox" name="delete[]" class="form-check-input" id="exampleCheck1" value="<?php echo $_smarty_tpl->tpl_vars['image']->value->id;?>
">
				<!--	<a class='btn btn-danger' href='<?php echo base_url();?>
admin/picture/delete_image/<?php echo $_smarty_tpl->tpl_vars['image']->value->id;?>
' name='submit'>Törlés</a>	-->
						
				</div>
				
			</div>
			
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</div>
		<div class="row justify-content-center p-5">
			<div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-primary" name="submit" value="Mentés" /></div>
		</div>


	</form>
</div><?php }
}
