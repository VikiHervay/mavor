<?php
/* Smarty version 3.1.33, created on 2020-02-21 12:44:27
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\admin\pages\login\login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4fc29b911e52_44800169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3767234c987f80d4bdf16e77ef1c03aec9ccb04e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\login\\login.tpl',
      1 => 1582284898,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4fc29b911e52_44800169 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand">Szatmarvill Kft</a>
</nav>

<?php if (isset($_SESSION['error'])) {?>
<div class="row py-3 justify-content-center">
    <div class="col-3">
        <div class="form-group alert alert-danger text-center" role="alert" >
            <?php echo $_SESSION['error'];?>

        </div>
    </div>
</div>

<?php }?>

<div class="row py-5 justify-content-center">
    <div class="col-3">
    
            <div class="form-group">
                <h3>Belépés</h3>
            </div>
        
            <form action="" method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Felhasználónév:</label>
                    <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Felhasználónév" value="<?php if ($_smarty_tpl->tpl_vars['username']->value) {
echo $_smarty_tpl->tpl_vars['username']->value;
}?>">
                </div>
                <?php if (isset($_SESSION['username'])) {?>
                <div class="form-group alert alert-danger text-center"  role="alert">
                        <?php echo $_SESSION['username'];?>

                </div>
                <?php }?>

                <div class="form-group">
                    <label for="exampleInputPassword1">Jelszó:</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Jelszó">
                </div>

                <?php if (isset($_SESSION['password'])) {?>
                <div class="form-group alert alert-danger text-center" role="alert">
                    <?php echo $_SESSION['password'];?>

                </div>
                <?php }?>

                <div class="form-group d-flex justify-content-center">
                    <button type="submit" name="submit" class="btn btn-primary">Belépés</button>
                </div>
            </form>
    </div>
</div><?php }
}
