<?php
/* Smarty version 3.1.33, created on 2020-03-30 14:51:16
  from 'D:\MyPrograms\xammp\htdocs\mavor\application\views\templates\front\front_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e81eb443a1e85_28100949',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '01e6934e86261f45fdc9bfbfcaf0b9693f2c25d0' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\mavor\\application\\views\\templates\\front\\front_layout.tpl',
      1 => 1585572674,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./_partials/head.tpl' => 1,
    'file:./_partials/sidebar.tpl' => 1,
    'file:./_partials/footer.tpl' => 1,
  ),
),false)) {
function content_5e81eb443a1e85_28100949 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $_smarty_tpl->_subTemplateRender("file:./_partials/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div class="wrapper">
       <!-- sidebar -->
        <?php $_smarty_tpl->_subTemplateRender("file:./_partials/sidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span></span>
                    </button>

                </div>
            </nav>
            <div class="container">
                <div id="alert_container" class="position-fixed" style="z-index: 1900; right: 1rem; top: 1rem;">
				<?php if (isset($_smarty_tpl->tpl_vars['notification']->value) && $_smarty_tpl->tpl_vars['notification']->value != '') {?>
					
                    <div class="alert alert-<?php echo $_smarty_tpl->tpl_vars['notification']->value['type'];?>
 alert-dismissible fade show" role="alert">
                        <strong><?php echo $_smarty_tpl->tpl_vars['notification']->value["label"];?>
</strong>
                        <?php echo $_smarty_tpl->tpl_vars['notification']->value["message"];?>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
				<?php }?>

			</div>
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['view']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
            </div>
        </div>
	</div>
	<?php $_smarty_tpl->_subTemplateRender("file:./_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
</html><?php }
}
