<?php
/* Smarty version 3.1.33, created on 2020-02-21 16:11:18
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\admin\pages\add_project\add_project.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4ff316a75a67_89384290',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cbce0e83096682c229a9be6e1314b5b249b3809b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\add_project\\add_project.tpl',
      1 => 1582297875,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4ff316a75a67_89384290 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="" method="POST" enctype="multipart/form-data">
<div class="row justify-content-center px-1 py-5">
    <?php if (isset($_SESSION['error'])) {?>
        <div class="col-3">
            <div class="form-group alert alert-danger" role="alert">
                    <?php echo $_SESSION['error'];?>

            </div>
        </div>
    <?php }?>
	
</div>

 <div class="row justify-content-center px-1">
    <div class="col col-md-6">Project neve:</div>
    <div class="col col-md-6 text-center form-group"><textarea name="project_name" class="form-control"></textarea></div>
     
</div>

<div class="row justify-content-center px-1">
    <div class="col col-md-6 ">Leírás:</div>
    <div class="col col-md-6 text-center form-group"><textarea rows="10" name="project_description" class="form-control"></textarea></div>
    
</div>


<div class="row justify-content-center px-1">
    <div class="col col-md-6 ">Képek hozzáadása:</div>
    <div class="col col-md-6 text-center form-group"><input type="file" name="userfile[]" size="20" multiple="multiple" /></div>
</div>

<div class="row justify-content-center p-5">
    <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-primary" name="submit" value="Mentés" /></div>
</div>

</form>
<?php }
}
