<?php
/* Smarty version 3.1.33, created on 2020-02-21 22:22:25
  from 'D:\MyPrograms\xammp\htdocs\szatmarvill\application\views\templates\front\pages\home\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e504a11d7cc99_75662288',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '73fadf758dee6197a52be643bec3ea9e5c2856c1' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\szatmarvill\\application\\views\\templates\\front\\pages\\home\\index.tpl',
      1 => 1582294968,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e504a11d7cc99_75662288 (Smarty_Internal_Template $_smarty_tpl) {
?><section id="about_us" class="py-5">
    <div class="container">

        <header>
            <h4 class="text-center text-md-left text-uppercase">SZATMÁRVILL Kereskedelmi Kft.</h4>
        </header>
        <div class="row">
            <div class="col-12 col-md-6">
                <p>
                    Az alább felsorolt tevékenységek elvégzéséhez rendelkezünk saját gépjárműparkkal és technikai eszközökkel, szakképzett vezetői és szerelői dolgozó állománnyal, önálló telephellyel.
                </p>
                <ul class="list-unstyled">
                    <li class="p-2">
                        <i class="fa fa-arrow-alt-circle-right mr-2 text-warning"></i>
                        Elektromos közmű építese
                    </li>
                    <li class="p-2">
                        <i class="fa fa-arrow-alt-circle-right mr-2 text-warning"></i>
                        Híradástechnikai célú közmű építés
                    </li>
                    <li class="p-2">
                        <i class="fa fa-arrow-alt-circle-right mr-2 text-warning"></i>
                        Földkábel és légvezeték hálózatok
                    </li>
                    <li class="p-2">
                        <i class="fa fa-arrow-alt-circle-right mr-2 text-warning"></i>
                        Transzformátor állomások építése
                    </li>
                    <li class="p-2">
                        <i class="fa fa-arrow-alt-circle-right mr-2 text-warning"></i>
                        Villamos hálózatok tervezése, kivitelezése
                    </li>
                    <li class="p-2">
                        <i class="fa fa-arrow-alt-circle-right mr-2 text-warning"></i>
                        Villamos hálózatok üzemeltetése és karbantartása
                    </li>
                </ul>
                <p>
                    A változó külső elvárásoknak megfelelően technikai felszereltségünket, szakképzett munkaerő állományunkat és infrastruktúránkat folyamatosan képesek vagyunk fejleszteni.
                </p>
            </div>

            <div class="col-12 col-md-6 d-flex flex-column justify-content-center">

                <div class="card border-0 mb-5" style="max-width: 540px;">
                    <div class="row no-gutters justify-content-center">
                      <div class="col-lg-4">
                        <img src="https://images.pexels.com/photos/230518/pexels-photo-230518.jpeg?crop=entropy&cs=srgb&fit=crop&fm=jpg&h=480&w=640" class="card-img" alt="...">
                      </div>
                    </div>
                </div>

                <div class="card border-0 mb-5" style="max-width: 540px;">
                    <div class="row no-gutters justify-content-center">
                      <div class="col-lg-4">
                        <img src="https://images.pexels.com/photos/257736/pexels-photo-257736.jpeg?crop=entropy&cs=srgb&fit=crop&fm=jpg&h=425&w=640" class="card-img" alt="...">
                      </div>
                    </div>
                </div>
                  
            </div>
        </div>
    </div>
</section>

<section id="services" class="py-5 bg-light">
    <div class="container">
        
        <header>
            <h4 class="text-center text-md-left text-uppercase">Tevékenységeink</h4>
        </header>

        <div class="services-row">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-4 offset-md-0 col-lg-4 offset-lg-0">
                    <div class="card text-center rounded-0 shadow-sm border-warning pt-1">
                        <div class="card-icon align-self-center align-items-center rounded-pill d-flex justify-content-center border-warning border shadow-sm text-warning">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="card-body pt-5">
                            <h5>
                                Híradástechnika
                            </h5>
                            <p class="card-text">
                                Elektromos és híradástechnikai célú közművek építése
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-8 offset-sm-2 col-md-4 offset-md-0 col-lg-4 offset-lg-0">
                    <div class="card text-center rounded-0 shadow-sm border-warning pt-1">
                        <div class="card-icon align-self-center align-items-center rounded-pill d-flex justify-content-center border-warning border shadow-sm text-warning">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="card-body pt-5">
                            <h5>
                                Villamos hálózatok
                            </h5>
                            <p class="card-text">
                                Villamos földkábel és légvezeték hálózatok tervezése, kivitelezése, üzemeltetése és karbantartása
                            </p>
                            
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-8 offset-sm-2 col-md-4 offset-md-0 col-lg-4 offset-lg-0">
                    <div class="card text-center rounded-0 shadow-sm border-warning pt-1">
                        <div class="card-icon align-self-center align-items-center rounded-pill d-flex justify-content-center border-warning border shadow-sm text-warning">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="card-body pt-5">
                            <h5>
                                Transzformátor
                            </h5>
                            <p class="card-text">
                                Középfeszültségű transzformátor állomások építése
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="offer" class="py-5 bg-warning">
    <div class="container">

        <div class="row">
            <!-- <div class="col-12 offset-0 col-sm-10 offset-sm-2 col-md-8 offset-md-4 col-lg-6 offset-lg-6"> -->
            <div class="col-12 col-md-6 d-flex align-item-center justify-content-center">
                <div class="image mt-4 mb-5 mb-md-0">
                    <img src="<?php echo base_url();?>
assets/images/content/chat-o.png" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <header>
                    <h4 class="text-center text-md-left text-uppercase">Kérjen ajánlatot</h4>
                </header>
                
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0 border-warning shadow-sm" placeholder="Név" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0 border-warning shadow-sm" placeholder="Email" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0 border-warning shadow-sm" placeholder="Telefonszám" />
                    </div>
                    <div class="form-group">
                        <textarea class="form-control rounded-0 border-warning shadow-sm" name="" id="" cols="" rows="" placeholder="Üzenet"></textarea>
                    </div>
                    <div class="form-group text-right">
                        <button class="btn btn-primary px-5">
                            <i class="fa fa-paper-plane mr-3 ml-n2"></i>
                            Elküld
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section id="projects" class="py-5">
    <div class="container">
        <header>
            <h4 class="text-center text-md-left text-uppercase">Projektjeink</h4>
        </header>

        <div class="row">

            <div class="col-12 col-sm-6 col-md-4 col-xl-3">
                <div class="card mb-3">
                    <img src="<?php echo base_url();?>
assets/images/content/projects-1.jpg" class="card-img" alt="...">
                    <div class="card-img-overlay overlay-bottom">
                      <h5 class="card-title">Villamos hálózat kivitelezés</h5>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>


<section id="contact_now">
    <div class="container">
        <div class="bg-primary text-white d-flex flex-column flex-md-row align-items-center justify-content-between">
            <div class="logo p-3">
                <img src="<?php echo base_url();?>
assets/images/szatmarvill-logo-white.svg" alt="Szatmárvill logo" class="d-inline-block align-top logo-white" width="160">
            </div>

            <div class="p-3 text-center">
                <h3 class="text-white mb-0">Hívjon minket bizalommal!</h3>
                <p class="font-italic mb-0">Kollégáink készséggel állank az Ön rendelkezésére!</p>
            </div>

            <div class="p-3">
                <button class="btn btn-warning text-primary font-weight-bold pulse-animation">
                    <i class="fa fa-phone mr-2 d-none d-lg-inline-block"></i>
                    Telefonálok most!
                </button>
            </div>

        </div>
    </div>
</section>

<section id="maps" class="mb-n2">
    <div class="row">
        <div class="col-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2672.7904496545634!2d22.33760451564206!3d47.94043697920813!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47387b127be334a9%3A0xe021c6ce844f4f52!2zTcOhdMOpc3phbGthLCBEw7N6c2EgR3nDtnJneSB1LiAxNzUsIDQ3MDA!5e0!3m2!1shu!2shu!4v1578498431674!5m2!1shu!2shu" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</section><?php }
}
