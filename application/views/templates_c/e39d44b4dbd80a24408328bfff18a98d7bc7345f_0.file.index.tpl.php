<?php
/* Smarty version 3.1.33, created on 2020-02-16 10:13:34
  from 'D:\MyPrograms\xammp\htdocs\keletagora\application\views\templates\front\pages\home\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4907bedcdac7_29245596',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e39d44b4dbd80a24408328bfff18a98d7bc7345f' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\keletagora\\application\\views\\templates\\front\\pages\\home\\index.tpl',
      1 => 1581700830,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4907bedcdac7_29245596 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- About Section -->
  <section class="page-section bg-primary p-1 p-md-5" id="rolunk">
       <div class="container">
      <h2 class="text-center mt-0">Rólunk</h2>
      <hr class="divider my-4">
      <div class="row">
        <div class="col-md-4 text-center">
          <div class="mt-5">
            <i class="icons fas fa-network-wired text-primary mb-4"></i>
            
            <p class="text-muted mb-0">Kiemelt szakkereskedője vagyunk a Schneider Electric Zrt-nek és a Legrand Zrt-nek a Schrack-nak. Ezenkívül forgalmazzuk a Finder, Gewiss, Moeller-Eaton, Siemens, ABB, Tracon, Hager, Weidmüller és -igény szerint- más termékeket is.</p>
         
          </div>
        </div>
        <div class="col-md-4 text-center">
          <div class="mt-5">
            <i class="icons fas fa-users text-primary mb-4"></i>
            <p class="text-muted mb-0">Ma már hat fővel dolgozunk, egy külön lámpaszaküzlettel várjuk Önöket a belvárosban is.</p>
          </div>
        </div>
        <div class="col-md-4 text-center">
          <div class="mt-5">
            <i class="icons far fa-lightbulb text-primary mb-4" ></i>
            
            <p class="text-muted mb-0"> Az Eglo Shop in shop csoport tagja vagyunk, és a Rábalux lámpák kiemelt forgalmazója, ezenkívül Globo, Luxera, Novodorsky, Searchlight,és még sok más lámpa is megtalálható nálunk. Várjuk Önöket folyamatosan változó akciókkal,és egyre bővülő kínálattal!</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Categories  Section -->
  <section class="page-section" id="kategoriak">
    <div class="container">
      <div class="row">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category', false, 'categ_index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['categ_index']->value => $_smarty_tpl->tpl_vars['category']->value) {
?>
            	
    		<div class="col-4 pt-4">
    		    <div class="card profile-card-5 h-100">
    		        <div class="card-img-block">
    		            <img class="card-img-top" src="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['category']->value->image;?>
" alt="Card image cap">
    		        </div>
                    <div class="card-body pt-0 text-center">
                        <a class="d-block card-title growing-link" href="<?php echo base_url();?>
kategoriak/<?php echo $_smarty_tpl->tpl_vars['category']->value->url;?>
/#lampak"><?php echo $_smarty_tpl->tpl_vars['category']->value->category_name;?>
</a>
                    </div>
                </div>
    		</div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
    </div>
  
  </section>

  <!-- Portfolio Section -->
  <section id="portfolio" class="bg-dark page-section p-0">
    
    <div class="row justify-content-center">
    
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src="<?php echo base_url();?>
assets/Carousel/1.jpg" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo base_url();?>
assets/Carousel/2.jpg" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo base_url();?>
assets/Carousel/3.jpg" alt="Third slide">
              </div>
            </div>
        
          </div>
      </div>
    
  </section>



  <!-- Contact Section -->
  <section class="page-section p-3" id="kapcsolat">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="mt-0">Elérhetőségeink</h2>
          <div class="d-flex justify-content-center">
          <a id="social" href="https://www.facebook.com/keletagora/" target="blank">
            <i class="fab fa-facebook-f"></i>
          </a>
        </div>
          <hr class="divider my-4">
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col text-center">
          <p><i class="fas fa-envelope-open"></i>
            email:	<a href="mailto:kelet-agora@szvill.t-online.hu">kelet-agora@szvill.t-online.hu</a><br>
                   <span style="margin-left: 90px;"><a href="mailto:egloshop.keletagora@gmail.com">egloshop.keletagora@gmail.com</a></span> 
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 text-center">
          <h3>Villamossági Szaküzlet</h3>
        
          
        <div>
           <p><a href="tel:+3644417859"><i class="fas fa-phone-square-alt px-1"></i>tel/fax: 06 (44) 417-859</a></p>
        </div>
            <iframe id="maps" class="border border-white rounded" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2672.5645043478034!2d22.315308515911724!3d47.944807079208566!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47387b64cc83f967%3A0xf08ba55b0d69f4e!2zTcOhdMOpc3phbGthLCBaw7ZsZGZhIHUuIDc0LCA0NzAw!5e0!3m2!1shu!2shu!4v1578404857365!5m2!1shu!2shu" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
        <div class="col-md-6 text-center">
          <h3>Lux Lámpa Szaküzlet</h3>
          
            <div>
              <p><a href="tel:+3644310844"><i class="fas fa-phone-square-alt px-1"></i>telefon: 06 (44) 310-844</a></p>
            </div>
            <iframe class=" border border-white rounded" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5344.376055316701!2d22.315783!3d47.952088!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47387b61ba5fca75%3A0x248ad1506277e1ad!2zTcOhdMOpc3phbGthLCDDgXJww6FkIHUuIDQsIDQ3MDA!5e0!3m2!1sen!2shu!4v1578405510912!5m2!1sen!2shu" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
    </div>
  </section>

<!--Nyitvatartási idő-->
  <section id="nyitvatartasi_ido" class="bg-light page-section">
    <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-8 text-center">
        <h2 class="mt-0">Nyitvatartási Idő: </h2>
        <hr class="divider my-4">
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col col-md-4">
        <table class="table table-borderless" >
          
          <tbody class="text-center">
          <tr>
            <td>Hétfő: </td>
            <td>07:30 - 17:00</td>
          </tr>
          <tr>
            <td>Kedd:</td>
            <td>07:30 - 17:00</td>
          </tr>
          <tr>
            <td>Szerda:</td>
            <td>07:30 - 17:00</td>
          </tr>
          <tr>
            <td>Csütörtök:</td>
            <td>07:30 - 17:00</td>
          </tr>
          <tr>
            <td>Péntek:</td>
            <td>07:30 - 17:00</td>
          </tr>
          <tr>
            <td>Szombat:</td>
            <td>07:30 - 13:00</td>
          </tr>
          <tr>
            <td>Vasárnap:</td>
            <td>Zárva</td>
          </tr>
        </tbody>
        </table>
      </div>
    </div>
    
    </div>
  </section>
<?php }
}
