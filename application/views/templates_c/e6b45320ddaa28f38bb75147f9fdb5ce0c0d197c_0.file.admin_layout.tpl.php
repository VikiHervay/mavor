<?php
/* Smarty version 3.1.33, created on 2020-03-30 09:02:38
  from 'D:\MyPrograms\xammp\htdocs\mavor\application\views\templates\admin\admin_layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e81998ee54950_37018459',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e6b45320ddaa28f38bb75147f9fdb5ce0c0d197c' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\mavor\\application\\views\\templates\\admin\\admin_layout.tpl',
      1 => 1585551502,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./template/head.tpl' => 1,
    'file:./template/sidebar.tpl' => 1,
    'file:./template/footer.tpl' => 1,
  ),
),false)) {
function content_5e81998ee54950_37018459 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $_smarty_tpl->_subTemplateRender("file:./template/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div class="wrapper">
       <!-- sidebar -->
        <?php $_smarty_tpl->_subTemplateRender("file:./template/sidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span></span>
                    </button>

                </div>
            </nav>
            <div class="container">
                <div id="alert_container" class="position-fixed" style="z-index: 1900; right: 1rem; top: 1rem;">
				<?php if (isset($_smarty_tpl->tpl_vars['notification']->value) && $_smarty_tpl->tpl_vars['notification']->value != '') {?>
					
                    <div class="alert alert-<?php echo $_smarty_tpl->tpl_vars['notification']->value['type'];?>
 alert-dismissible fade show" role="alert">
                        <strong><?php echo $_smarty_tpl->tpl_vars['notification']->value["label"];?>
</strong>
                        <?php echo $_smarty_tpl->tpl_vars['notification']->value["message"];?>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
				<?php }?>

			</div>
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['view']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
            </div>
        </div>
	</div>
	<?php $_smarty_tpl->_subTemplateRender("file:./template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
</html><?php }
}
