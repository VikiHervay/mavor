<?php
/* Smarty version 3.1.33, created on 2020-02-20 16:02:54
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\admin\login\login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4e9f9e0c7188_86112729',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bcd6691f112b3fb4db88b9b97d9dae017548f007' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\login\\login.tpl',
      1 => 1582206536,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4e9f9e0c7188_86112729 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand">Kelet Agóra</a>
</nav>
<div class="row py-5 justify-content-center">
    <div class="col-6">
    
    <div class="form-group">
            <h3>Belépés</h3>
    </div>
    <div style="color: red;"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</div>
       
       <form action="admin/auth/login" method="POST">
        <div class="form-group">
            <label for="exampleInputEmail1">Felhasználónév:</label>
            <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Felhasználónév">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Jelszó:</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Jelszó">
        </div>
        <div class="form-group d-flex justify-content-center">
             <button type="submit" class="btn btn-primary">Belépés</button>
        </div>
        </form>
    </div>
</div><?php }
}
