<?php
/* Smarty version 3.1.33, created on 2020-02-21 10:35:15
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\front\_partials\header_carousel.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4fa453bbf3b8_13305643',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c0cabc59da603d8855de1cb7b58119e9d4302ea0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\front\\_partials\\header_carousel.tpl',
      1 => 1582277700,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4fa453bbf3b8_13305643 (Smarty_Internal_Template $_smarty_tpl) {
?> <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="<?php echo base_url();?>
assets/images/slider/slider_1.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
        <img src="<?php echo base_url();?>
assets/images/slider/slider_2.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
        <img src="<?php echo base_url();?>
assets/images/slider/slider_3.jpg" class="d-block w-100" alt="...">
    </div>
     <div class="carousel-item">
        <img src="<?php echo base_url();?>
assets/images/slider/slider_4.jpg" class="d-block w-100" alt="...">
    </div>
</div>
<a class="carousel-control-prev" href="#header_carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#header_carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a><?php }
}
