<?php
/* Smarty version 3.1.33, created on 2020-03-31 09:08:20
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\pages\add\add_course1.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e82ec64ed4fb4_85125397',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b4964e8d5ac5296fa2445a23731e19ea2954401a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\add\\add_course1.tpl',
      1 => 1585637755,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e82ec64ed4fb4_85125397 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="" method="POST" enctype="multipart/form-data">
        <div class="row justify-content-center m-4">
                <h4>Kurzus feltöltése 3/1.</h4>
        </div>
    <div class="row justify-content-center">
        <div class="col col-lg-6 text-center form-group">
            <div class="my-card border p-5">
                <label>Kurzus neve:</label>
                <textarea name="course_name" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['course_name']->value)) {
echo $_smarty_tpl->tpl_vars['course_name']->value;
}?></textarea>
            </div>
        </div>
        <div class="col col-lg-6 text-center form-group">
            <div class="my-card border p-5">
                <label>Kredit értéke:</label>
                <textarea name="course_credit" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['course_credit']->value)) {
echo $_smarty_tpl->tpl_vars['course_credit']->value;
}?></textarea>
            </div>
        </div>
    </div>

    <div class="row justify-content-center px-1">
        <div class="col col-md-6 text-center form-group">
            <div class="my-card border p-5">
                <label>Kurzuskérdések feltöltése:</label>
                <input type="file" name="course_questions" size="20" multiple="multiple" />
            </div>
        </div>

        <div class="col col-md-6 text-center form-group">
            <div class="my-card border p-5">
                    <label> Videók feltöltése: </label>
                    <input type="file" name="videos[]" size="20" multiple="multiple" />

            </div>    
        </div>
    </div>
    
    
    <div class="row justify-content-center p-5">
        <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-success btn-lg" name="submit_add_course1" value="Mentés" /></div>
	</div>
















</form><?php }
}
