<?php
/* Smarty version 3.1.33, created on 2020-02-13 19:05:48
  from 'D:\MyPrograms\xammp\htdocs\keletagora\application\views\templates\front\pages\categories\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e458ffc6546a7_33666260',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2c3d07d102cdc9dbe4b58037ec4e3823760ca3a9' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\keletagora\\application\\views\\templates\\front\\pages\\categories\\index.tpl',
      1 => 1581614414,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e458ffc6546a7_33666260 (Smarty_Internal_Template $_smarty_tpl) {
?>  <section class="page-section bg-light" id="lampak">
    <div class="container">
      <div class="row">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['lamps']->value, 'lamp', false, 'lamp_index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['lamp_index']->value => $_smarty_tpl->tpl_vars['lamp']->value) {
?>
            	
    		<div class="col-12 col-md-4 mt-3 mb-3">
    		    <div class="card profile-card-5 h-100">
    		        <div class="card-img-block h-100">
    		            <img class="card-img-top " src="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['lamp']->value->image;?>
" alt="Card image cap">
    		        </div>
                    <div class="card-body pt-0 text-center h-100">
                        <h5 class="d-block card-title"> <?php echo $_smarty_tpl->tpl_vars['lamp']->value->lamp_name;?>
 </h5>
                    </div>
                    <div class="card-body pt-0 h-100">
                        <span class="item-number">
                            <h5 class="d-block small" >Cikkszám: <?php echo $_smarty_tpl->tpl_vars['lamp']->value->item_number;?>
 </h5>
                        </span>
                        <span class="price">
                            <h5 class="pull-right d-block">Ár: <?php echo $_smarty_tpl->tpl_vars['lamp']->value->price;?>
 Ft </h5>
                        </span>
                    </div>
                    <div class="card-body pt-0 text-center h-100">
                        <form method="POST" action="<?php echo base_url();?>
buying/#form"><input type="hidden" name="lamp_id" value="<?php echo $_smarty_tpl->tpl_vars['lamp']->value->id;?>
"/><input class="d-block btn buy-btn-move w-100" type="submit" value='Megveszem' /></form>
                    </div>
                    
                </div>
    		</div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
    </div>
  
  </section><?php }
}
