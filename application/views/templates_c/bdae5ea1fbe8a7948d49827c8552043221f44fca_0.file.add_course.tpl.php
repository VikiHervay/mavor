<?php
/* Smarty version 3.1.33, created on 2020-03-29 09:03:54
  from 'D:\MyPrograms\xammp\htdocs\mavor\application\views\templates\admin\pages\add\add_course.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e80485abc6344_17536870',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bdae5ea1fbe8a7948d49827c8552043221f44fca' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\add\\add_course.tpl',
      1 => 1585465430,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e80485abc6344_17536870 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="" method="POST" enctype="multipart/form-data">
    <div class="row justify-content-center px-1 py-5">
        <?php if (isset($_SESSION['error'])) {?>
            <div class="col-6">
                <div class="form-group alert alert-danger text-center alert-dismissible fade show" role="alert">
                        <?php echo $_SESSION['error'];?>

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
            </div>
        <?php }?>
        <?php if (isset($_SESSION['success'])) {?>
            <div class="col-3">
                <div class="form-group alert alert-success alert-dismissible fade show" role="alert">
                    <?php echo $_SESSION['success'];?>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        <?php }?>
    </div>
    <div class="row">
        <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                <label>Kurzus neve:</label>
                <textarea name="name" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['name']->value)) {
echo $_smarty_tpl->tpl_vars['name']->value;
}?></textarea>
            </div>
        </div>
        <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                <label>Kredit értéke:</label>
                <textarea name="credit" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['credit']->value)) {
echo $_smarty_tpl->tpl_vars['credit']->value;
}?></textarea>
            </div>
        </div>

    </div>


    <div class="row  justify-content-center px-1">
            <div class="col col-md-6 text-center form-group">
                <div class="my-card border p-5">
                    <label>Kurzuskérdések feltöltése:</label>
                    <input type="file" name="userfile" size="20" multiple="multiple" />
                </div>
            </div>

    </div>
      <div class="row justify-content-center">

        <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                    <label> Videó címe: </label>
                    <textarea name="name" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['name']->value)) {
echo $_smarty_tpl->tpl_vars['name']->value;
}?></textarea>
            </div>    
        </div>
         <div class="col col-md-6 text-center form-group">
                <div id="uploads"></div>
                <div class="dropzone" id="dropzone">
                    Drop files fere to upload
                </div>
        </div>
      
       
    </div>

	<div class="row justify-content-center p-5">
        <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-success btn-lg" name="submit" value="Mentés" /></div>
	</div>

</form><?php }
}
