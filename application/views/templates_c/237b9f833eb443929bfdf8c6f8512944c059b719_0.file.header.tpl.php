<?php
/* Smarty version 3.1.33, created on 2020-02-21 14:36:03
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\admin\template\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4fdcc3284898_44203869',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '237b9f833eb443929bfdf8c6f8512944c059b719' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\template\\header.tpl',
      1 => 1582292097,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4fdcc3284898_44203869 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?php echo base_url();?>
admin">Szatmárvill Kft.</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/projektek">Projektek</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url();?>
admin/projektek_hozzaad">Projekt hozzáadása</a>
      </li>
    </ul>  
      
    <a class="btn btn-outline-warning my-2 my-sm-0 mx-2" type="submit" href="<?php echo base_url();?>
admin/auth/password_change">Jelszó megváltoztatása</a>

      <a class="btn btn-outline-danger my-2 my-sm-0" type="submit" href="<?php echo base_url();?>
admin/auth/logout">Kijelentkezés</a>
       
  </div>

</nav><?php }
}
