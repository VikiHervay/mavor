<?php
/* Smarty version 3.1.33, created on 2020-02-17 13:56:05
  from 'C:\xampp\htdocs\keletagora\application\views\templates\front\_partials\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4a8d6543ce22_82260328',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bfe5ad42df0eac4b0d50fbe83e854d68e74a105b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\keletagora\\application\\views\\templates\\front\\_partials\\header.tpl',
      1 => 1581944156,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4a8d6543ce22_82260328 (Smarty_Internal_Template $_smarty_tpl) {
?><body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      
      <a class="navbar-brand js-scroll-trigger" href="#">
        <img src="<?php echo base_url();?>
assets/img/KeletAgoraLogo_246x160.png" alt="logo" style="width:100px;">
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#rolunk">Rólunk</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#kategoriak">Kategóriák</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#bolt">Boltunk</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#kapcsolat">Kapcsolat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#nyitvatartasi_ido">Nyitvatartási idő</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" target="_blank" href="<?php echo base_url();?>
assets/Adatkezelesi_tajekoztato_Kelet-Agora_kft.pdf">GDPR</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" target="_blank" href="<?php echo base_url();?>
assets/Kelet_agora_aszf.pdf">ÁSZF</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Masthead -->
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Üdvözöljük a Kelet-Agóra Kft weboldalán!</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#rolunk">Tudjon meg többet</a>
        </div>
      </div>
    </div>
  </header><?php }
}
