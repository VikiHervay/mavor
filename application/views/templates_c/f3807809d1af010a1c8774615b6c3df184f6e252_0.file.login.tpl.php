<?php
/* Smarty version 3.1.33, created on 2020-03-25 11:01:04
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\pages\login\login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e7b2be0845131_22306265',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3807809d1af010a1c8774615b6c3df184f6e252' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\login\\login.tpl',
      1 => 1585128614,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e7b2be0845131_22306265 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand">MAVOR</a>
</nav>

<?php if (isset($_SESSION['error'])) {?>
<div class="row py-3 justify-content-center">
    <div class="col-3">
        <div class="form-group alert alert-danger text-center" role="alert" >
            <?php echo $_SESSION['error'];?>

        </div>
    </div>
</div>

<?php }?>

<div class="row py-5 justify-content-center">
    <div class="col-3">
    
            <div class="form-group">
                <h3>Belépés</h3>
            </div>
        
            <form action="" method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Felhasználónév:</label>
                    <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Felhasználónév" value="<?php if ($_smarty_tpl->tpl_vars['username']->value) {
echo $_smarty_tpl->tpl_vars['username']->value;
}?>">
                </div>
                <?php if (isset($_SESSION['username'])) {?>
                <div class="form-group alert alert-danger text-center"  role="alert">
                        <?php echo $_SESSION['username'];?>

                </div>
                <?php }?>

                <div class="form-group">
                    <label for="exampleInputPassword1">Jelszó:</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Jelszó">
                </div>

                <?php if (isset($_SESSION['password'])) {?>
                <div class="form-group alert alert-danger text-center" role="alert">
                    <?php echo $_SESSION['password'];?>

                </div>
                <?php }?>

                <div class="form-group d-flex justify-content-center">
                    <button type="submit" name="submit" class="btn btn-primary">Belépés</button>
                </div>
            </form>
    </div>
</div><?php }
}
