<?php
/* Smarty version 3.1.33, created on 2020-02-21 12:42:38
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\admin\pages\add_news\add_news.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4fc22e5e78f0_80078322',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4ed8d439cb23d1c24e8fb377fc043ce705f54613' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\add_news\\add_news.tpl',
      1 => 1582269231,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4fc22e5e78f0_80078322 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="admin/news/add" method="POST">
<div class="row justify-content-center px-1 py-5">
    
	<!-- error messages -->
	
</div>

 <div class="row justify-content-center px-1">
    <div class="col col-md-6">Cím:</div>
    <div class="col col-md-6 text-center form-group"><textarea name="news_title" class="form-control"></textarea></div>
     
</div>

<div class="row justify-content-center px-1">
    <div class="col col-md-6 ">Leírás:</div>
    <div class="col col-md-6 text-center form-group"><textarea rows="10" name="news_body" class="form-control"></textarea></div>
    
</div>


<div class="row justify-content-center px-1">
    <div class="col col-md-6 ">Képek hozzáadása:</div>
    <div class="col col-md-6 text-center form-group"><input type="file" name="userfile[]" size="20" multiple="multiple" /></div>
</div>

<div class="row justify-content-center p-5">
    <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-primary" name="submit" value="Mentés" /></div>
</div>

</form>
<?php }
}
