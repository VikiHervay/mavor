<?php
/* Smarty version 3.1.33, created on 2020-02-14 07:31:22
  from 'D:\MyPrograms\xammp\htdocs\keletagora\application\views\templates\front\_partials\head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e463eba92c4a0_74401321',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2604b05305976e114aa1fa5164760f02f954838b' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\keletagora\\application\\views\\templates\\front\\_partials\\head.tpl',
      1 => 1581661876,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e463eba92c4a0_74401321 (Smarty_Internal_Template $_smarty_tpl) {
?>  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Kelet Agora</title>

  <!-- Font Awesome Icons -->
  <link href="<?php echo base_url();?>
assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="<?php echo base_url();?>
assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="<?php echo base_url();?>
assets/css/creative.css" rel="stylesheet">

  <!-- Facebook Pixel Code -->
<?php echo '<script'; ?>
>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window,document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '1269679173220678'); 
  fbq('track', 'PageView');
  <?php echo '</script'; ?>
>
  <noscript>
   <img height="1" width="1" 
  src="https://www.facebook.com/tr?id=1269679173220678&ev=PageView
  &noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->
  
  <!-- Title icon-->
  <link rel="icon" type="image/ico" href="<?php echo base_url();?>
assets/img/KeletAgoraLogo_246x160.png"/>

  
    <?php echo '<script'; ?>
>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    <?php echo '</script'; ?>
>
  <?php }
}
