<?php
/* Smarty version 3.1.33, created on 2020-03-31 14:25:52
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\pages\add\add_course2.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e8336d03b83f8_35801534',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'be5387c37bfcf9a642324bbd127d80d784731dfa' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\add\\add_course2.tpl',
      1 => 1585637755,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e8336d03b83f8_35801534 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="<?php echo base_url();?>
admin/kurzusok_hozzaad_2/<?php echo $_smarty_tpl->tpl_vars['course_id']->value;?>
" method="POST">
         <div class="row flex-column align-items-center justify-content-center m-4">
             <div class="col text-center">
                <h4>Kurzus feltöltése 3/2.</h4>
            </div>
            <div class="col text-center">
                <p>Rendezze kívánt sorrendbe a videókat</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-md-6">
                 <div class="my-card border p-5">
                   <div class="row justify-content-center">
                          <h3>Videók</h3>
                    </div>

                    <ul id="drag_container" class="form-group">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['videos']->value, 'video', false, 'video_key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['video_key']->value => $_smarty_tpl->tpl_vars['video']->value) {
?>
                            
                            <li draggable="true" class="draggable list-group-item"  data-value="<?php echo $_smarty_tpl->tpl_vars['video']->value->video_id;?>
"><?php echo $_smarty_tpl->tpl_vars['video']->value->video_name[0]->video_name;?>
</li>
                            <input type="hidden" class="hidden_inputs" name="video_ids[]" data-value="<?php echo $_smarty_tpl->tpl_vars['video']->value->video_id;?>
" value="<?php echo $_smarty_tpl->tpl_vars['video']->value->video_id;?>
"/>
                            
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </ul>
                    <div class="row justify-content-center">
                        <button id="ok" class="btn btn-success" type="submit" name="order_videos">OK</button>
                    </div>
                </div>
            </div>
        </div>

        

</form>

<?php }
}
