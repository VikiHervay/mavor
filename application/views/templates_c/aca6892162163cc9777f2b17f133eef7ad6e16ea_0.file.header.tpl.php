<?php
/* Smarty version 3.1.33, created on 2020-02-21 11:26:28
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\front\_partials\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4fb0540712a2_68910406',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aca6892162163cc9777f2b17f133eef7ad6e16ea' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\front\\_partials\\header.tpl',
      1 => 1582280786,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4fb0540712a2_68910406 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="container">
    <div class="row">
        <div class="col-2">
            <!-- <img src="images/szatmarvill-logo.svg" alt="Szatmárvill logo" class="img-fluid pt-1"> -->
        </div>
        <div class="col-10 ">
            <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a class="nav-link active" href="tel:44500108">
                      <i class="fa fa-phone mr-1"></i>
                      44/500-108
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="mailto:info@szatmarvill.hu">
                      <i class="fa fa-envelope mr-1"></i>
                      info@szatmarvill.hu
                  </a>
                </li>
                
                <!--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-globe-europe"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Magyar</a>
                        <a class="dropdown-item" href="#">English</a>
                    </div>-->
                </li>
              </ul>
        </div>
    </div>
</div><?php }
}
