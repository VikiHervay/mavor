<?php
/* Smarty version 3.1.33, created on 2020-02-21 18:14:08
  from 'D:\MyPrograms\xammp\htdocs\szatmarvill\application\views\templates\admin\pages\login\password_change.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e500fe051d387_82929787',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a2553b051e532a9e3a5069cdddafc2e9e5f3c8f0' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\login\\password_change.tpl',
      1 => 1582294968,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e500fe051d387_82929787 (Smarty_Internal_Template $_smarty_tpl) {
?>
    <?php if (isset($_SESSION['error'])) {?>
<div class="row py-3 justify-content-center">
    <div class="col-3">
        <div class="form-group alert alert-danger text-center" role="alert">
                <?php echo $_SESSION['error'];?>

        </div>
    </div>
</div>
    <?php }?>

<div class="row py-5 justify-content-center">
    <div class="col-6">
    <div class="form-group">
            <h3>Jelszó megváltoztatása</h3>
    </div>

      <form action="" method="POST">
        <div class="form-group">
            <label for="oldPassword">Régi jelszó:</label>
            <input type="password" name="oldPassword" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Régi jelszó">
       

        </div>
        <div class="form-group">
            <label for="newsPassword1">Új jelszó:</label>
            <input type="password" name="newPassword1" class="form-control" id="exampleInputPassword1" placeholder="Új jelszó">
           


        </div>
        <div class="form-group">
            <label for="newPassword2">Új jelszó ismét:</label>
            <input type="password" name="newPassword2" class="form-control" id="exampleInputPassword1" placeholder="Új jelszó újra">
           


        </div>
        <div class="form-group d-flex justify-content-center">
             <button type="submit" name="submit" class="btn btn-primary">Ok</button>
        </div>
        </form>
    </div>
</div><?php }
}
