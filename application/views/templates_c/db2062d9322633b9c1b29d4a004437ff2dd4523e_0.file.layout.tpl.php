<?php
/* Smarty version 3.1.33, created on 2020-02-12 07:16:59
  from 'D:\MyPrograms\xammp\htdocs\keletagora\application\views\templates\front\layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e43985ba701a0_05100505',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'db2062d9322633b9c1b29d4a004437ff2dd4523e' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\keletagora\\application\\views\\templates\\front\\layout.tpl',
      1 => 1581487155,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./_partials/head.tpl' => 1,
    'file:./_partials/header.tpl' => 1,
    'file:./_partials/footer.tpl' => 1,
    'file:./_partials/scripts.tpl' => 1,
  ),
),false)) {
function content_5e43985ba701a0_05100505 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $_smarty_tpl->_subTemplateRender("file:./_partials/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div id="page_wrapper" class="d-flex flex-column justify-content-between">
        <div id="header_wrapper">
			<?php $_smarty_tpl->_subTemplateRender("file:./_partials/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </div>

        <div id="content_wrapper" class="">
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['view']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>

        <div id="footer_wrapper" class="pt-5 text-white">
			<?php $_smarty_tpl->_subTemplateRender("file:./_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender("file:./_partials/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
</html><?php }
}
