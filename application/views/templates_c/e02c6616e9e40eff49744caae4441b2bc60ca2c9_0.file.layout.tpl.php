<?php
/* Smarty version 3.1.33, created on 2020-02-21 10:22:00
  from 'C:\xampp\htdocs\szatmarvill\application\views\templates\front\layout.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4fa13868fd75_38682137',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e02c6616e9e40eff49744caae4441b2bc60ca2c9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\szatmarvill\\application\\views\\templates\\front\\layout.tpl',
      1 => 1582276806,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./_partials/head.tpl' => 1,
    'file:./_partials/header.tpl' => 1,
    'file:./_partials/header_nav.tpl' => 1,
    'file:./_partials/header_carousel.tpl' => 1,
    'file:./_partials/footer.tpl' => 1,
    'file:./_partials/scripts.tpl' => 1,
  ),
),false)) {
function content_5e4fa13868fd75_38682137 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php $_smarty_tpl->_subTemplateRender("file:./_partials/head.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


</head>
<body style="overflow-x: hidden">
    <div id="page_wrapper">
        <div id="header_wrapper">
            <header>
                <div id="header_contact" class="pt-2">
                    <?php $_smarty_tpl->_subTemplateRender("file:./_partials/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                </div>
                <div id="header_nav" class=" navbar-light bg-light">    
                    <?php $_smarty_tpl->_subTemplateRender("file:./_partials/header_nav.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                </div>
            </header>
            <div id="header_carousel" class="carousel slide" data-ride="carousel">
                <?php $_smarty_tpl->_subTemplateRender("file:./_partials/header_carousel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            </div>
        </div>

        <div id="content_wrapper" class="">
			<?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['view']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>

        <div id="footer_wrapper" class="bg-info">
			<?php $_smarty_tpl->_subTemplateRender("file:./_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </div>
	</div>
	
	<?php $_smarty_tpl->_subTemplateRender("file:./_partials/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
</html><?php }
}
