<?php
/* Smarty version 3.1.33, created on 2020-02-20 18:38:14
  from 'D:\MyPrograms\xammp\htdocs\szatmarvill\application\views\templates\admin\pages\add_news\add_news.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4ec406bca5d4_25418113',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '22043f26f48eadebb36d8f3f93d1e44a96738d14' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\add_news\\add_news.tpl',
      1 => 1582219629,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4ec406bca5d4_25418113 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="admin/news/add" method="POST">
<div class="row justify-content-center px-1 py-5">
    
	<!-- error messages -->
	
</div>

 <div class="row justify-content-center px-1">
    <div class="col col-md-6">Cím:</div>
    <div class="col col-md-6 text-center form-group"><textarea name="news_title" class="form-control"></textarea></div>
     
</div>

<div class="row justify-content-center px-1">
    <div class="col col-md-6 ">Leírás:</div>
    <div class="col col-md-6 text-center form-group"><textarea rows="10" name="news_body" class="form-control"></textarea></div>
    
</div>


<div class="row justify-content-center px-1">
    <div class="col col-md-6 ">Képek hozzáadása:</div>
    <div class="col col-md-6 text-center form-group"><input type="file" name="userfile[]" size="20" multiple="multiple" /></div>
</div>

<div class="row justify-content-center p-5">
    <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-primary" name="submit" value="Mentés" /></div>
</div>

</form>
<?php }
}
