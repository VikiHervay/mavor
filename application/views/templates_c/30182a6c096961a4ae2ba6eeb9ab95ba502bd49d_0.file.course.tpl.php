<?php
/* Smarty version 3.1.33, created on 2020-03-31 14:24:17
  from 'C:\xampp\htdocs\mavor\application\views\templates\admin\pages\content\course.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e833671c0ef27_67847494',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '30182a6c096961a4ae2ba6eeb9ab95ba502bd49d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mavor\\application\\views\\templates\\admin\\pages\\content\\course.tpl',
      1 => 1585637755,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e833671c0ef27_67847494 (Smarty_Internal_Template $_smarty_tpl) {
?><table class="table">
	<thead>
	<tr>
		<th  scope="col">Kurzusok</th>
		<th class='text-center' scope="col">Szerkesztés</th>
		<th class='text-center' scope="col">Törlés</th>
	</tr>
	</thead>
	<tbody>


	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['courses']->value, 'course', false, 'index');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['index']->value => $_smarty_tpl->tpl_vars['course']->value) {
?>
	<tr> 
		<td><?php echo $_smarty_tpl->tpl_vars['course']->value->course_name;?>
</td>  
		    
		<td class='text-center'><a class='btn btn-warning' role='button' href='<?php echo base_url();?>
admin/courses/edit/<?php echo $_smarty_tpl->tpl_vars['course']->value->id;?>
'><i class='far fa-edit'></i></a></td>		 
		<td class='text-center'><a class='btn btn-danger' type='button' href="<?php echo base_url();?>
admin/courses/delete/<?php echo $_smarty_tpl->tpl_vars['course']->value->id;?>
"><i class='fas fa-trash'></i></a></td>
	</tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

	</tbody>
</table>


<?php }
}
