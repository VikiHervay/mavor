<?php
/* Smarty version 3.1.33, created on 2020-02-16 10:13:34
  from 'D:\MyPrograms\xammp\htdocs\keletagora\application\views\templates\front\_partials\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4907bec5f728_43382152',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '00352bd4e7eb187f85fa5e2fcb85411775f89c23' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\keletagora\\application\\views\\templates\\front\\_partials\\header.tpl',
      1 => 1581700830,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4907bec5f728_43382152 (Smarty_Internal_Template $_smarty_tpl) {
?><body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      
      <a class="navbar-brand js-scroll-trigger" href="#">
        <img src="<?php echo base_url();?>
assets/img/KeletAgoraLogo_246x160.png" alt="logo" style="width:100px;">
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#rolunk">Rólunk</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#kategoriak">Kategóriák</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#portfolio">Boltunk</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#kapcsolat">Kapcsolat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url();?>
#nyitvatartasi_ido">Nyitvatartási idő</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" target="_blank" href="<?php echo base_url();?>
assets/Adatkezelesi_tajekoztato_Kelet-Agora_kft.pdf">GDPR</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Masthead -->
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Üdvözöljük a Kelet-Agóra Kft weboldalán!</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#rolunk">Tudjon meg többet</a>
        </div>
      </div>
    </div>
  </header><?php }
}
