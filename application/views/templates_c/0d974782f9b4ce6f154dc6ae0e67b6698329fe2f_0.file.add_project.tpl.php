<?php
/* Smarty version 3.1.33, created on 2020-02-21 21:54:17
  from 'D:\MyPrograms\xammp\htdocs\szatmarvill\application\views\templates\admin\pages\add_project\add_project.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e504379cf0e67_01615035',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d974782f9b4ce6f154dc6ae0e67b6698329fe2f' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\szatmarvill\\application\\views\\templates\\admin\\pages\\add_project\\add_project.tpl',
      1 => 1582318456,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e504379cf0e67_01615035 (Smarty_Internal_Template $_smarty_tpl) {
?><form action="" method="POST" enctype="multipart/form-data">
<div class="row justify-content-center px-1 py-1">
    <?php if (isset($_SESSION['error'])) {?>
        <div class="col-6">
            <div class="form-group alert alert-danger" role="alert">
                    <?php echo $_SESSION['error'];?>

            </div>
        </div>
    <?php }?>
    <?php if (isset($_SESSION['img_is_toobig'])) {?>
        <div class="col-3">
            <div class="form-group alert alert-danger" role="alert">
                <?php echo $_SESSION['img_is_toobig'];?>

            </div>
        </div>
    <?php }?>
</div>

 <div class="row justify-content-center px-1">
    <div class="col col-md-6">Project neve:</div>
    <div class="col col-md-6 text-center form-group"><textarea name="project_name" class="form-control"></textarea></div>
     
</div>

<div class="row justify-content-center px-1">
    <div class="col col-md-6 ">Leírás:</div>
    <div class="col col-md-6 text-center form-group"><textarea rows="10" name="project_description" class="form-control"></textarea></div>
    
</div>


<div class="row justify-content-center px-1">
    <div class="col col-md-6 ">Képek hozzáadása:</div>
    <div class="col col-md-6 text-center form-group"><input type="file" name="userfile[]" size="20" multiple="multiple" /></div>
</div>

<div class="row justify-content-center p-5">
    <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-primary" name="submit" value="Mentés" /></div>
</div>

</form>
<?php }
}
