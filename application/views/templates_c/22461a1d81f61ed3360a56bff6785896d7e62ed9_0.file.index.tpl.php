<?php
/* Smarty version 3.1.33, created on 2020-02-16 13:18:14
  from 'D:\MyPrograms\xammp\htdocs\keletagora\application\views\templates\front\pages\buying\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e4933064da0a3_08210116',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '22461a1d81f61ed3360a56bff6785896d7e62ed9' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\keletagora\\application\\views\\templates\\front\\pages\\buying\\index.tpl',
      1 => 1581855490,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e4933064da0a3_08210116 (Smarty_Internal_Template $_smarty_tpl) {
?>
<section class="page-section bg-light" id="form">
 <div class="container">
 <div class="row">
        <div class="col-lg-8">
          <form id="contactForm" name="sentMessage" novalidate="novalidate" action="<?php echo base_url();?>
buying/send_email/#form" method="POST">
            <div class="row justify-content-center">
              <div class="col-md-6">
                  <!-- Success message -->
                <div class="alert alert-success" role="alert" style="<?php echo $_smarty_tpl->tpl_vars['success_visiblity']->value;?>
">
                    <p class="text-center"><?php if ($_smarty_tpl->tpl_vars['success']->value) {?> <?php echo $_smarty_tpl->tpl_vars['success']->value;?>
   <?php }?></p>
                 
                </div>
                  <!-- error message -->
                <div class="alert alert-danger"  role="alert" style="<?php echo $_smarty_tpl->tpl_vars['error_visibility']->value;?>
">
                    <p class="text-center"><?php if ($_smarty_tpl->tpl_vars['error']->value) {?> <?php echo $_smarty_tpl->tpl_vars['error']->value;?>
   <?php }?></p>
                 
                </div>
                <div class="form-group">
                  <input class="form-control" id="name" name="name" type="text" placeholder="Név *" value="<?php if ($_smarty_tpl->tpl_vars['name']->value) {
echo $_smarty_tpl->tpl_vars['name']->value;
}?>">
                 
                </div>
                <div class="form-group">
                  <input class="form-control" id="email" name="email" type="email" placeholder="Email cím *" value="<?php if ($_smarty_tpl->tpl_vars['email']->value) {
echo $_smarty_tpl->tpl_vars['email']->value;
}?>">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="phone" name="phone" type="tel" placeholder="Telefonszám *" value="<?php if ($_smarty_tpl->tpl_vars['phone']->value) {
echo $_smarty_tpl->tpl_vars['phone']->value;
}?>">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="city" name="city" type="text" placeholder="Város *" value="<?php if ($_smarty_tpl->tpl_vars['city']->value) {
echo $_smarty_tpl->tpl_vars['city']->value;
}?>">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="street" name="street" type="text" placeholder="Utca *" value="<?php if ($_smarty_tpl->tpl_vars['street']->value) {
echo $_smarty_tpl->tpl_vars['street']->value;
}?>">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="house_number" name="house_number" type="text" placeholder="Házszám, ajtó *" value="<?php if ($_smarty_tpl->tpl_vars['house_number']->value) {
echo $_smarty_tpl->tpl_vars['house_number']->value;
}?>">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control pt-5 pr-5 pb-5"  id="message" name="message" type="text" placeholder="Megjegyzés" value="<?php if ($_smarty_tpl->tpl_vars['address']->value) {
echo $_smarty_tpl->tpl_vars['message']->value;
}?>">
                  <p class="help-block text-danger"></p>
                </div>
              
                <!--  <div class="form-check">
                  <input class="form-check-input" type="radio" name="paying" id="transfer" value="transfer" checked>
                  <label class="form-check-label d-inline" for="transfer">Utalással szeretnék fizetni</label><p class="d-inline small"  data-toggle="tooltip" data-placement="right" title="Ebben az esetben elküldjük önnek emailben a számlaszámot!" style="text-decoration: underline;">( ? )</p>
                </div>-->

                <div class="form-check">
                  <input class="form-check-input" type="radio" name="paying" id="cash-on-delivery" value="cod" checked>
                <label class="form-check-label" for="cash-on-delivery">Utánvét</label> 
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" name="ASZF" id="ASZF">
                  <a href="Kelet_agora_aszf.pdf" target="blank"> <label class="form-check-label" for="ASZF">Általános szerződési feltételek</label></a>
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" name="GDPR" id="GDPR">
                  <a  target="_blank" href="<?php echo base_url();?>
assets/Adatkezelesi_tajekoztato_Kelet-Agora_kft.pdf"><label class="form-check-label" for="GDPR">Adatkezelési tájékoztató</label></a>
                </div>
                <input name="lamp_id" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['lamp_id']->value;?>
"/>
              </div>
              <div class="clearfix"></div>

              <div class="col-lg-12 text-center mt-3">
                <div id="success"></div>
                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit" name="submit">Küldés</button>
              </div>

            </div>
          </form>
        </div>

        <div class='d-none d-md-block col-lg-4'>
            <div class="card profile-card-5 ">
    		        <div class="card-img-block ">
    		            <img class="card-img-top " src="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['img_src']->value;?>
" alt="Card image cap">
    		        </div>
                    <div class="card-body pt-0 text-center ">
                        <h5 class="d-block card-title"> <?php echo $_smarty_tpl->tpl_vars['lamp']->value->lamp_name;?>
 </h5>
                    </div>
                    <div class="card-body pt-0 ">
                        <span class="item-number">
                            <h5 class="d-block small" >Cikkszám: <?php echo $_smarty_tpl->tpl_vars['lamp']->value->item_number;?>
 </h5>
                        </span>
                        <span class="price">
                            <h5 class="pull-right d-block">Ár: <?php echo $_smarty_tpl->tpl_vars['lamp']->value->price;?>
 Ft </h5>
                        </span>
                    </div>                    
                </div>
        </div>
      </div>
    </div>

</section><?php }
}
