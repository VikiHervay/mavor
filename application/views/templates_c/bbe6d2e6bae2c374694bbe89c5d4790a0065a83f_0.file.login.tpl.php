<?php
/* Smarty version 3.1.33, created on 2020-03-30 12:36:02
  from 'D:\MyPrograms\xammp\htdocs\mavor\application\views\templates\front\pages\login\login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5e81cb9298a9b4_64654169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bbe6d2e6bae2c374694bbe89c5d4790a0065a83f' => 
    array (
      0 => 'D:\\MyPrograms\\xammp\\htdocs\\mavor\\application\\views\\templates\\front\\pages\\login\\login.tpl',
      1 => 1585564467,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5e81cb9298a9b4_64654169 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand">MAVOR</a>
</nav>

<?php if (isset($_SESSION['error'])) {?>
<div class="row py-3 justify-content-center">
    <div class="col-3">
        <div class="form-group alert alert-danger text-center" role="alert" >
            <?php echo $_SESSION['error'];?>

        </div>
    </div>
</div>

<?php }?>

<div class="row py-5 justify-content-center">
    <div class="col-3">
    
            <div class="form-group">
                <h3>Belépés</h3>
            </div>
        
            <form action="" method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Kód:</label>
                    <input type="text" name="id" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Felhasználónév" value="<?php if ($_smarty_tpl->tpl_vars['username']->value) {
echo $_smarty_tpl->tpl_vars['username']->value;
}?>">
                </div>
                <?php if (isset($_SESSION['id'])) {?>
                <div class="form-group alert alert-danger text-center"  role="alert">
                        <?php echo $_SESSION['id'];?>

                </div>
                <?php }?>

                <div class="form-group d-flex justify-content-center">
                    <button type="submit" name="submit" class="btn btn-primary">Belépés</button>
                </div>
            </form>
    </div>
</div><?php }
}
