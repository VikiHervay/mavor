<div class="row">
    <div class="col col-md-6">
        <ul id="drag_container" class="form-group">
            <li draggable="true" class="draggable list-group-item" value="1" data-value="1">Apples</li>
            <li draggable="true" class="draggable list-group-item" value="1" data-value="2">Oranges</li>
            <li draggable="true" class="draggable list-group-item" value="1" data-value="3">Bananas</li>
            <li draggable="true" class="draggable list-group-item" value="1" data-value="4">Strawberries</li>
        </ul>
        <button id="ok">OK</button>
    </div>
</div>