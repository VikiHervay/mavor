<table class="table">
	<thead>
	<tr>
		<th  scope="col">Kurzusok</th>
		<th class='text-center' scope="col">Szerkesztés</th>
		<th class='text-center' scope="col">Törlés</th>
	</tr>
	</thead>
	<tbody>


	{foreach $courses as $index => $course}
	<tr> 
		<td>{$course->course_name}</td>  
		    
		<td class='text-center'><a class='btn btn-warning' role='button' href='{base_url()}admin/courses/edit/{$course->id}'><i class='far fa-edit'></i></a></td>		 
		<td class='text-center'><a class='btn btn-danger' type='button' href="{base_url()}admin/courses/delete/{$course->id}"><i class='fas fa-trash'></i></a></td>
	</tr>
	{/foreach}

	</tbody>
</table>


