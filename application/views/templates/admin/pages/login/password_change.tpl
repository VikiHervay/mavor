
    {if isset($smarty.session.error)}
<div class="row py-3 justify-content-center">
    <div class="col-3">
        <div class="form-group alert alert-danger text-center" role="alert">
                {$smarty.session.error}
        </div>
    </div>
</div>
    {/if}

<div class="row py-5 justify-content-center">
    <div class="col-6">
    <div class="form-group">
            <h3>Jelszó megváltoztatása</h3>
    </div>

      <form action="" method="POST">
        <div class="form-group">
            <label for="oldPassword">Régi jelszó:</label>
            <input type="password" name="oldPassword" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Régi jelszó">
       

        </div>
        <div class="form-group">
            <label for="newsPassword1">Új jelszó:</label>
            <input type="password" name="newPassword1" class="form-control" id="exampleInputPassword1" placeholder="Új jelszó">
           


        </div>
        <div class="form-group">
            <label for="newPassword2">Új jelszó ismét:</label>
            <input type="password" name="newPassword2" class="form-control" id="exampleInputPassword1" placeholder="Új jelszó újra">
           


        </div>
        <div class="form-group d-flex justify-content-center">
             <button type="submit" name="submit" class="btn btn-primary">Ok</button>
        </div>
        </form>
    </div>
</div>