<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand">MAVOR</a>
</nav>

{if isset($smarty.session.error)}
<div class="row py-3 justify-content-center">
    <div class="col-3">
        <div class="form-group alert alert-danger text-center" role="alert" >
            {$smarty.session.error}
        </div>
    </div>
</div>

{/if}

<div class="row py-5 justify-content-center">
    <div class="col-3">
    
            <div class="form-group">
                <h3>Belépés</h3>
            </div>
        
            <form action="" method="POST">
                <div class="form-group">
                    <label for="exampleInputEmail1">Felhasználónév:</label>
                    <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Felhasználónév" value="{if $username}{$username}{/if}">
                </div>
                {if isset($smarty.session.username)}
                <div class="form-group alert alert-danger text-center"  role="alert">
                        {$smarty.session.username}
                </div>
                {/if}

                <div class="form-group">
                    <label for="exampleInputPassword1">Jelszó:</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Jelszó">
                </div>

                {if isset($smarty.session.password)}
                <div class="form-group alert alert-danger text-center" role="alert">
                    {$smarty.session.password}
                </div>
                {/if}

                <div class="form-group d-flex justify-content-center">
                    <button type="submit" name="submit" class="btn btn-primary">Belépés</button>
                </div>
            </form>
    </div>
</div>