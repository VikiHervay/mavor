<!--<form action="{base_url()}admin/kurzusok_hozzaad_3/{$course_id}" method="POST">-->
         <div class="row flex-column align-items-center justify-content-center m-4">
             <div class="col text-center">
                <h4>Kurzus feltöltése 3/3.</h4>
            </div>
            <div class="col text-center">
                <p>Válassza ki melyik videóhoz melyik kérdés tartozik és kattintson az OK gombra</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-md-6">
                 <div class="my-card border p-5">
                   <div class="row justify-content-center">
                          <h3>Videók</h3>
                    </div>
                     <div class="row justify-content-center">
                           {foreach $videos as $video_key => $video}
                                <div class="col-6">
                                     <p class="list-group-item line-break picked-video" data-value="{$video->video_id}">{$video->video_name[0]->video_name}</p>
                                </div>
                                <div class="col-6 d-flex align-items-center justify-content-center">
                                 <input type="checkbox" name="video_id" class="list-group-item form-check-input picked-video-id"  value="{$video->video_id}">
                                </div>
                            {/foreach}
                    </div>
                </div>
            </div>
             <div class="col col-md-6">
                 <div class="my-card border p-5">
                   <div class="row justify-content-center">
                          <h3>Kérdések</h3>
                    </div>

                    <div class="row justify-content-center">
                           {foreach $questions as $q_key => $question}
                                <div class="col-6">
                                     <p class="list-group-item line-break picked-question" data-value="{$question->id}">{$question->question}</p>
                                </div>
                                <div class="col-6 d-flex align-items-center justify-content-center">
                                 <input type="checkbox" name="question_id[]" class="list-group-item form-check-input picked-question-id"  value="{$question->id}">
                                </div>
                            {/foreach}
                    </div>
                  
                </div>
            </div>
        </div>
        <div class="row justify-content-center m-3">
            <button id="match" class="btn btn-success" type="submit" name="match_questions">OK</button>
        </div>
        

<!--</form>-->