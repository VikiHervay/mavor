<form action="{base_url()}admin/kurzusok_hozzaad_2/{$course_id}" method="POST">
         <div class="row flex-column align-items-center justify-content-center m-4">
             <div class="col text-center">
                <h4>Kurzus feltöltése 3/2.</h4>
            </div>
            <div class="col text-center">
                <p>Rendezze kívánt sorrendbe a videókat</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-md-6">
                 <div class="my-card border p-5">
                   <div class="row justify-content-center">
                          <h3>Videók</h3>
                    </div>

                    <ul id="drag_container" class="form-group">
                        {foreach $videos as $video_key => $video}
                            
                            <li draggable="true" class="draggable list-group-item"  data-value="{$video->video_id}">{$video->video_name[0]->video_name}</li>
                            <input type="hidden" class="hidden_inputs" name="video_ids[]" data-value="{$video->video_id}" value="{$video->video_id}"/>
                            
                        {/foreach}
                    </ul>
                    <div class="row justify-content-center">
                        <button id="ok" class="btn btn-success" type="submit" name="order_videos">OK</button>
                    </div>
                </div>
            </div>
        </div>

        

</form>

