<form action="" method="POST" enctype="multipart/form-data">
        <div class="row justify-content-center m-4">
                <h4>Kurzus feltöltése 3/1.</h4>
        </div>
    <div class="row justify-content-center">
        <div class="col col-lg-6 text-center form-group">
            <div class="my-card border p-5">
                <label>Kurzus neve:</label>
                <textarea name="course_name" class="form-control">{if isset($course_name)}{$course_name}{/if}</textarea>
            </div>
        </div>
        <div class="col col-lg-6 text-center form-group">
            <div class="my-card border p-5">
                <label>Kredit értéke:</label>
                <textarea name="course_credit" class="form-control">{if isset($course_credit)}{$course_credit}{/if}</textarea>
            </div>
        </div>
    </div>

    <div class="row justify-content-center px-1">
        <div class="col col-md-6 text-center form-group">
            <div class="my-card border p-5">
                <label>Kurzuskérdések feltöltése:</label>
                <input type="file" name="course_questions" size="20" multiple="multiple" />
            </div>
        </div>

        <div class="col col-md-6 text-center form-group">
            <div class="my-card border p-5">
                    <label> Videók feltöltése: </label>
                    <input type="file" name="videos[]" size="20" multiple="multiple" />

            </div>    
        </div>
    </div>
    
    
    <div class="row justify-content-center p-5">
        <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-success btn-lg" name="submit_add_course1" value="Mentés" /></div>
	</div>
















</form>