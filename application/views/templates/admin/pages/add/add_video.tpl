<form action="" method="POST" enctype="multipart/form-data">

<div class="row justify-content-center px-1 py-5">
    {if isset($smarty.session.error)}
        <div class="col-6">
            <div class="form-group alert alert-danger text-center alert-dismissible fade show" role="alert">
                    {$smarty.session.error}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
            </div>
        </div>
    {/if}
    {if isset($smarty.session.success)}
        <div class="col-3">
            <div class="form-group alert alert-success alert-dismissible fade show" role="alert">
                {$smarty.session.success}
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    {/if}
</div>
    <div class="row justify-content-center">

        <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                    <label> Videó címe: </label>
                    <textarea name="name" class="form-control">{if isset($name)}{$name}{/if}</textarea>
            </div>    
        </div>
         <div class="col col-md-6 text-center form-group">

                <div id="uploads"></div>
                <div class="dropzone" id="dropzone">
                    Drop files fere to upload
                </div>

            {* <div class="my-card border">
                    <label>Videó feltöltése:</label><br>
                    <input type="file" name="userfile" size="20" multiple="multiple" />
            </div> *}
        </div>
      
       
    </div>
    
    <div class="row  justify-content-center px-1">
         <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                <label>Videóhoz tartozó kérdés kiválasztása: </label><br>
                <ul id="question-list">
                    {foreach $questions as $index => $question }
                       <li><a class="dropdown-question" data-value="{$question->id}">{$question->question}</a></li>  
                    {/foreach} 
                </ul>
            </div>
        </div>
        <input type="hidden" name="question_id" id="question_id" value="0"/>

         <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                <label>Melyik kurzushoz tartozik a videó: </label><br>
                <ul id="question-list">
                    {foreach $courses as $index => $course }
                       <li><a class="dropdown-course" data-value="{$course->id}">{$course->course_name}</a></li>  
                    {/foreach} 
                </ul>
            </div>
        </div>
        <input type="hidden" name="course_id" id="course_id" value="0"/>
        
    </div>


    <div class="row justify-content-center p-5">
        <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-success btn-lg" name="submit" value="Mentés" /></div>
    </div>
</div>

</form>

{* <form action="videos/fileupload" class="dropzone" id="fileupload"></form> *}
