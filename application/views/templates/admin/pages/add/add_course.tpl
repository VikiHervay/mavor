<form action="" method="POST" enctype="multipart/form-data">
    <div class="row justify-content-center px-1 py-5">
        {if isset($smarty.session.error)}
            <div class="col-6">
                <div class="form-group alert alert-danger text-center alert-dismissible fade show" role="alert">
                        {$smarty.session.error}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                </div>
            </div>
        {/if}
        {if isset($smarty.session.success)}
            <div class="col-3">
                <div class="form-group alert alert-success alert-dismissible fade show" role="alert">
                    {$smarty.session.success}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        {/if}
    </div>
    <div class="row">
        <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                <label>Kurzus neve:</label>
                <textarea name="name" class="form-control">{if isset($name)}{$name}{/if}</textarea>
            </div>
        </div>
        <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                <label>Kredit értéke:</label>
                <textarea name="credit" class="form-control">{if isset($credit)}{$credit}{/if}</textarea>
            </div>
        </div>

    </div>


    <div class="row  justify-content-center px-1">
            <div class="col col-md-6 text-center form-group">
                <div class="my-card border p-5">
                    <label>Kurzuskérdések feltöltése:</label>
                    <input type="file" name="userfile" size="20" multiple="multiple" />
                </div>
            </div>

    </div>
      <div class="row justify-content-center">

        <div class="col col-md-6 text-center form-group">
            <div class="my-card border">
                    <label> Videó címe: </label>
                    <textarea name="name" class="form-control">{if isset($name)}{$name}{/if}</textarea>
            </div>    
        </div>
         <div class="col col-md-6 text-center form-group">
                <div id="uploads"></div>
                <div class="dropzone" id="dropzone">
                    Drop files fere to upload
                </div>
        </div>
      
       
    </div>

	<div class="row justify-content-center p-5">
        <div class="col col-md-6 text-center form-group"><input type="submit" class="btn btn-success btn-lg" name="submit" value="Mentés" /></div>
	</div>

</form>