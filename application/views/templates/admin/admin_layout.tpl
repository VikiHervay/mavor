<!DOCTYPE html>
<html lang="en">
<head>
	{include file="./template/head.tpl"}
</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div class="wrapper">
       <!-- sidebar -->
        {include file="./template/sidebar.tpl"}
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span></span>
                    </button>

                </div>
            </nav>
            <div class="container">
                <div id="alert_container" class="position-fixed" style="z-index: 1900; right: 1rem; top: 1rem;">
				{if isset($notification) && $notification != ""}
					
                    <div class="alert alert-{$notification['type']} alert-dismissible fade show" role="alert">
                        <strong>{$notification["label"]}</strong>
                        {$notification["message"]}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
				{/if}

			</div>
			{include file=$view}
            </div>
        </div>
	</div>
	{include file="./template/footer.tpl"}
</body>
</html>