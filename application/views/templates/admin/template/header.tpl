<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="{base_url()}admin">MAVOR</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="{base_url()}admin/videok">Videók</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{base_url()}admin/videok_hozzaad">Videók hozzáadása</a>
      </li>
     <!-- <li class="nav-item">
        <a class="nav-link" href="{base_url()}admin/kerdesek">Kurzus kérdéssor</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{base_url()}admin/kerdesek_hozzaad">Kérdéssor hozzáadása</a>
      </li>-->
      <li class="nav-item">
        <a class="nav-link" href="{base_url()}admin/kurzusok">Kurzusok</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="{base_url()}admin/kurzusok_hozzaad">Kurzusok hozzáadása</a>
      </li>
    </ul>  
      
    <a class="btn btn-outline-warning my-2 my-sm-0 mx-2" type="submit" href="{base_url()}admin/auth/password_change">Jelszó megváltoztatása</a>

    <a class="btn btn-outline-danger my-2 my-sm-0" type="submit" href="{base_url()}admin/auth/logout">Kijelentkezés</a>
       
  </div>

</nav>