<nav id="sidebar">
        <div class="sidebar-header d-flex justify-content-center">
            <h3>MAVOR</h3>
        </div>

        <ul class="list-unstyled components">
          
            <li class="active">
                <a class="sidebar-link" href="{base_url()}admin/kurzusok_hozzaad">Kurzus hozzáadás</a>
            </li>
             {* <li>
                <a class="sidebar-link" href="{base_url()}admin/videok_hozzaad">Videó hozzáadás</a>
            </li> *}
            {* <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="#">Page 1</a>
                    </li>
                    <li>
                        <a href="#">Page 2</a>
                    </li>
                    <li>
                        <a href="#">Page 3</a>
                    </li>
                </ul>
            </li> *}
            <li>
                <a class="sidebar-link" href="{base_url()}admin/kurzusok">Kurzusok</a>
            </li>
            <li>
                <a class="sidebar-link" href="{base_url()}admin/videok">Videók</a>
            </li>
        </ul>
    </nav>
