<!DOCTYPE html>
<html lang="en">
<head>
	{include file="./template/head.tpl"}
</head>
<body class="position-relative" data-spy="scroll" data-target=".fixed-top" data-offset="100">
    <div id="page_wrapper" class="d-flex flex-column justify-content-between">
  
        <div id="content_wrapper" class="">
			{include file=$view}
        </div>

        <div id="footer_wrapper" class="pt-5 text-white">
			{include file="./template/footer.tpl"}
        </div>
	</div>
</body>
</html>
