 <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="{base_url()}assets/images/slider/slider_1.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
        <img src="{base_url()}assets/images/slider/slider_2.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
        <img src="{base_url()}assets/images/slider/slider_3.jpg" class="d-block w-100" alt="...">
    </div>
     <div class="carousel-item">
        <img src="{base_url()}assets/images/slider/slider_4.jpg" class="d-block w-100" alt="...">
    </div>
</div>
<a class="carousel-control-prev" href="#header_carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#header_carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>