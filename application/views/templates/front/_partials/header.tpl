<div class="container">
    <div class="row">
        <div class="col-2">
            <!-- <img src="images/szatmarvill-logo.svg" alt="Szatmárvill logo" class="img-fluid pt-1"> -->
        </div>
        <div class="col-10 ">
            <ul class="nav justify-content-end">
                <li class="nav-item">
                  <a class="nav-link active" href="tel:44500108">
                      <i class="fa fa-phone mr-1"></i>
                      44/500-108
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="mailto:info@szatmarvill.hu">
                      <i class="fa fa-envelope mr-1"></i>
                      info@szatmarvill.hu
                  </a>
                </li>
                
                <!--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-globe-europe"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Magyar</a>
                        <a class="dropdown-item" href="#">English</a>
                    </div>-->
                </li>
              </ul>
        </div>
    </div>
</div>