<div class="container">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#">
            <img src="{base_url()}assets/images/szatmarvill-logo.svg" alt="Szatmárvill logo" class="d-inline-block align-top" width="160">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fa fa-home"></i>
            </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="#about_ut">Bemutatkozás <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#projects">Projektek</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#services">Tevékenységeink</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact_now">Kapcsolat</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-warning btn-quote" href="#offer">
                    Kérjen ajánlatot
                </a>
            </li>
            </ul>
        </div>
    </nav>
</div>