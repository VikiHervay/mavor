<nav id="sidebar">
        <div class="sidebar-header d-flex justify-content-center">
            <h3>MAVOR</h3>
        </div>

        <ul class="list-unstyled components">
          
            <li class="list-group-item">
                <video class="side-video" muted preload="true" width="180"  height="135"  volume="0">
                    <source src="{base_url()}assets/videos/kurzus_1.mp4" type="video/mp4">
                </video>
            </li>
            <li class="list-group-item">
                <video class="side-video" muted preload="true" width="180"  height="135"  volume="0">
                    <source src="{base_url()}assets/videos/kurzus_2.mp4" type="video/mp4">
                </video>
            </li>
            <li class="list-group-item">
                <video class="side-video" muted preload="true" width="180"  height="135"  volume="0">
                    <source src="{base_url()}assets/videos/kuruzs_3.mp4" type="video/mp4">
                </video>
            </li>
            
        </ul>
    </nav>
