<?php
    class Question_model extends CI_Model{

    public function __construct(){

        
    }

    public function create_question($data){

            $this->db->insert('questions', $data);

            return $this->db->insert_id();        

    }        

    public function read_question($where = 0, $what = 0){

        if ($what === 0 && $where === 0) {
                
                $this->db->order_by('id', 'DESC');
                return $this->db->get('questions')->result();

        }else{
                $this->db->order_by('id', 'DESC');
            return $this->db->get_where('questions', array($where => $what))->result();
        }

    }

    public function update_question($where, $what, $data){

        $this->db->where($where, $what);
        $this->db->update('questions', $data);
    }

    public function delete_question($where, $what){

        $this->db->where($where, $what);
        $this->db->delete('questions');
    }



}

?>