<?php
    class Coursecontent_model extends CI_Model{

    public function __construct(){

        
    }

    public function create_coursecontent($data){

            $this->db->insert('course_contents', $data);

            return $this->db->insert_id();        

    }        

    public function read_coursecontent($where = 0, $what = 0){

        if ($what === 0 && $where === 0) {
                
                $this->db->order_by('id', 'DESC');
                return $this->db->get('course_contents')->result();

        }else{
                $this->db->order_by('id', 'DESC');
            return $this->db->get_where('course_contents', array($where => $what))->result();
        }

    }

    public function update_coursecontent($where, $what, $data){

        $this->db->where($where, $what);
        $this->db->update('course_contents', $data);
    }

    public function delete_coursecontent($where, $what){

        $this->db->where($where, $what);
        $this->db->delete('course_contents');
    }



}

?>