<?php
    class Course_model extends CI_Model{

    public function __construct(){

        
    }

    public function create_course($data){

            $this->db->insert('courses', $data);

            return $this->db->insert_id();        

    }        

    public function read_course($where = 0, $what = 0){

        if ($what === 0 && $where === 0) {
                
                $this->db->order_by('id', 'DESC');
                return $this->db->get('courses')->result();

        }else{
                $this->db->order_by('id', 'DESC');
            return $this->db->get_where('courses', array($where => $what))->result();
        }

    }

    public function update_course($where, $what, $data){

        $this->db->where($where, $what);
        $this->db->update('courses', $data);
    }

    public function delete_course($where, $what){

        $this->db->where($where, $what);
        $this->db->delete('courses');
    }



}

?>