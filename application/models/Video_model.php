<?php
    class Video_model extends CI_Model{

    public function __construct(){

        
    }

    public function create_video($data){

            $this->db->insert('videos', $data);

            return $this->db->insert_id();        

    }        

    public function read_video($where = 0, $what = 0){

        if ($what === 0 && $where === 0) {
                
                $this->db->order_by('id', 'DESC');
                return $this->db->get('videos')->result();

        }else{
                $this->db->order_by('id', 'DESC');
            return $this->db->get_where('videos', array($where => $what))->result();
        }

    }

    public function update_video($where, $what, $data){

        $this->db->where($where, $what);
        $this->db->update('videos', $data);
    }

    public function delete_video($where, $what){

        $this->db->where($where, $what);
        $this->db->delete('videos');
    }



}

?>