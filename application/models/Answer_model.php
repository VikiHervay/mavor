<?php
    class Answer_model extends CI_Model{

    public function __construct(){

        
    }

    public function create_answer($data){

            $this->db->insert('answers', $data);

            return $this->db->insert_id();        

    }        

    public function read_answer($where = 0, $what = 0){

        if ($what === 0 && $where === 0) {
                
                $this->db->order_by('id', 'DESC');
                return $this->db->get('answers')->result();

        }else{
                $this->db->order_by('id', 'DESC');
            return $this->db->get_where('answers', array($where => $what))->result();
        }

    }

    public function update_answer($where, $what, $data){

        $this->db->where($where, $what);
        $this->db->update('answers', $data);
    }

    public function delete_answer($where, $what){

        $this->db->where($where, $what);
        $this->db->delete('answers');
    }



}

?>