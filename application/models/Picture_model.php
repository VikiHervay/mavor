<?php
    class Picture_model extends CI_Model{

        public function __construct(){

            
        }
        
        public function save_picture($imageSrc, $id, $where = '0'){

            $srcArray = explode('/', $imageSrc);
                $assetsIndex = 0;
                for ($i=0; $i < count($srcArray); $i++) { 
                    if ($srcArray[$i] == "assets") {
                        $assetsIndex = $i;
                    }
                }
                $goodSrc = array_slice($srcArray, $assetsIndex);

                $src = implode("/",$goodSrc);
                
                if ($where == 'project_images') {
                    $data = array(
                        'project_id' => $id,
                        'src' => $src
                    );
                }
                    

                 $this->db->insert($where, $data);

        }

        public function get_picture($table, $where, $what){

             $this->db->where($where, $what);
             return $this->db->get($table)->result();

        }

        public function get_all_pictures($table){

            return $this->db->get($table)->result();
            
        }
       
        public function update_picture($table, $where, $what, $data){

            $this->db->where($where, $what);
            $this->db->update($table, $data);
       
        }

        public function delete_picture($table, $where, $what){

            $this->db->where($where, $what);
            return $this->db->delete($table);
        }



}

?>