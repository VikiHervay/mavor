<?php
    class Questionvideo_model extends CI_Model{

    public function __construct(){

        
    }

    public function create_questionvideo($data){

            $this->db->insert('question_video', $data);

            return $this->db->insert_id();        

    }        

    public function read_questionvideo($where = 0, $what = 0){

        if ($what === 0 && $where === 0) {
                
                $this->db->order_by('id', 'DESC');
                return $this->db->get('question_video')->result();

        }else{
                $this->db->order_by('id', 'DESC');
            return $this->db->get_where('question_video', array($where => $what))->result();
        }

    }

    public function update_questionvideo($where, $what, $data){

        $this->db->where($where, $what);
        $this->db->update('question_video', $data);
    }

    public function delete_questionvideo($where, $what){

        $this->db->where($where, $what);
        $this->db->delete('question_video');
    }



}

?>