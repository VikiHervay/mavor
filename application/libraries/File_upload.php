<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class File_upload{
    
    protected $CI;
    public function __construct(){

    }

    public function HandlingExcel($path, $course_id){
        $CI  =&get_instance();
        header('Content-type: text/html; charset=UTF-8');
        $myfile = fopen($path, "r") or die("Unable to open");
    
        fgets($myfile);
        while(!feof($myfile)) {
            $sor = fgets($myfile);
            $sorTomb = explode(';',$sor);
        
            if (count($sorTomb) > 1 ) {
                if ($sorTomb[1] != "final") {
                    $question_data = [
                        "course_id" => $course_id,
                        "question" => $sorTomb[2],
                        "video_id" => null,
                        "question_type" => "block"
                    ];
                    $q_id = $CI->question_model->create_question($question_data);
                    $answers = [
                        "correct" => $sorTomb[3],
                        "incorrect1" => $sorTomb[4],
                        "incorrect2" => $sorTomb[5],
                        "incorrect3" => $sorTomb[6]
                    ];
                    foreach ($answers as $key => $value) {
                        if ($key == "correct") {
                            $answer_data = [
                                "answer" =>$value,
                                "question_id" => $q_id,
                                'answer_correct' => true
                            ];
                        $CI->answer_model->create_answer($answer_data);
                        }else{
                            $answer_data = [
                                "answer" =>$value,
                                "question_id" => $q_id,
                                'answer_correct' => false
                            ];
                        $CI->answer_model->create_answer($answer_data);
                        }
                    }
                }else{
                    $question_data = [
                        "course_id" => $course_id,
                        "question" => $sorTomb[2],
                        "video_id" => null,
                        "question_type" => "final"
                    ];
                    $q_id = $CI->question_model->create_question($question_data);
                    $answers = [
                        "correct" => $sorTomb[3],
                        "incorrect1" => $sorTomb[4],
                        "incorrect2" => $sorTomb[5],
                        "incorrect3" => $sorTomb[6]
                    ];
                    foreach ($answers as $key => $value) {
                        if ($key == "correct") {
                            $answer_data = [
                                "answer" =>$value,
                                "question_id" => $q_id,
                                'answer_correct' => true
                            ];
                        $CI->answer_model->create_answer($answer_data);
                        }else{
                            $answer_data = [
                                "answer" =>$value,
                                "question_id" => $q_id,
                                'answer_correct' => false
                            ];
                        $CI->answer_model->create_answer($answer_data);
                        }
                    }
                
                }
            
            }
    
        }
        fclose($myfile);
        unlink($path);
    }


    public function HandlingVideos($files, $course_id){
        $CI  =&get_instance();
       
        $upload_count = count($files['name']);

        for ($i=0; $i < $upload_count; $i++) { 
            header('Content-Type: text/html; charset=utf-8');
            $fileName = $files['name'][$i];
            $fileExt = explode('.', $fileName);
            $extention = strtolower(end($fileExt));
            $video_name = Withoutaccents($fileExt[0]);
            $fileDestination = 'assets/videos/'.$video_name.'.'.$extention;
            move_uploaded_file($files['tmp_name'][$i], $fileDestination);

            $videodata = [
                'video_name' => $video_name,
                'url'=> $fileDestination,
                'position' => 0
            ];
            
            $video_id = $CI->video_model->create_video($videodata);

            $data_to_course_content = [
                'course_id' => $course_id,
                'video_id' => $video_id
            ];

            $CI->coursecontent_model->create_coursecontent($data_to_course_content);

        }
    }

}
?>