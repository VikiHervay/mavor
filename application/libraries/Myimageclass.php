<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Myimageclass{
    
    protected $CI;
    public function __construct(){

    }


    public function SaveImagesFixedHeightNoVertical($sideFolder, $mainFolder, $where = 0, $inputFiles){
        $CI  =&get_instance();
        ini_set("memory_limit","256M");

        if (!file_exists('assets/'.$mainFolder)) {
           mkdir('assets/'.$mainFolder);
        }

        if (!file_exists('assets/'.$mainFolder.'/full')) {
            mkdir('assets/'.$mainFolder.'/full');
        }

        if (!file_exists('assets/'.$mainFolder.'/smaller')) {
        mkdir('assets/'.$mainFolder.'/smaller');
        }

        if (!file_exists('assets/'.$mainFolder.'/full/'.$sideFolder)) {
            mkdir('assets/'.$mainFolder.'/full/'.$sideFolder);
        }

        if (!file_exists('assets/'.$mainFolder.'/smaller/'.$sideFolder)) {
            mkdir('assets/'.$mainFolder.'/smaller/'.$sideFolder);
        }

        
        $error_messages = array();
    
        $config['upload_path'] = './assets/'.$mainFolder.'/full/'.$sideFolder;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '800000';

        $errors = [];
        $files = $inputFiles;
        
        $upload_count = count( $files['userfile']['name'] );
   
        $extraVar = 0;
        $CI->load->library('image_lib');
        for( $i = 0; $i < $upload_count; $i++ )
        {
            date_default_timezone_set('UTC');
            $fileNameDate = date('Y-m-d-s');
            //get the extension pt1
            $ext = explode('.', $files['userfile']['name'][$i]);
            //get the extension pt2 (jpg stb)
            $actualExt = strtolower(end($ext));

            $fileName = $fileNameDate.$extraVar.".".$actualExt;

                $_FILES['userfile'] = [
                    'name'     => $fileName,
                    'type'     => $files['userfile']['type'][$i],
                    'tmp_name' => $files['userfile']['tmp_name'][$i],
                    'error'    => $files['userfile']['error'][$i],
                    'size'     => $files['userfile']['size'][$i]
                ];

                
                $CI->load->library('upload', $config);

                if ($files['userfile']['size'][$i] > 20000000) {
                     
                    if (!isset($error_messages['toobig'])) {
                       $error_messages['toobig'] = array();
                    }

                    array_push($error_messages['toobig'], $files['userfile']['name'][$i]);
                   
                    
                }else{

                            // Use upload library for file validation
                            if( $CI->upload->do_upload('userfile') )
                            {
                                //Upload was successful
                                $imageSrc = $CI->upload->data('full_path');
                            
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $imageSrc;
                                $config['create_thumb'] = FALSE;
                                $config['maintain_ratio'] = FALSE;
                                $config['quality'] = '100%';


                                $orgfile = $imageSrc;
                                $info = getimagesize($orgfile);
                                // echo "<pre>";
                                // print_r($info);

                                $width = $info[0];
                                $height = $info[1];
                            
                                        if ($height > $width) {
                                            
                                            if (!isset($error_messages['vertical'])) {
                                                $error_messages['vertical'] = [];
                                            }
                                            array_push($error_messages['vertical'], $files['userfile']['name'][$i]);
                                            
                                            $full_path = 'assets/'.$mainFolder.'/full/'.$sideFolder.'/'.$_FILES['userfile']['name'];
                                            DeletePictureFromFolder($full_path);   

                                                
                                            

                                        }else{
                                            
                                            $config['width'] = 300;
                                            $config['height'] = 250;
                                            $newPath= './assets/'.$mainFolder.'/smaller/'.$sideFolder.'/'.$_FILES['userfile']['name'];


                                            $newPath= './assets/'.$mainFolder.'/smaller/'.$sideFolder.'/'.$_FILES['userfile']['name'];
                                            $config['new_image'] = $newPath;
                                        
                                            $CI->image_lib->initialize($config);
                                            $CI->image_lib->resize();
                                            $CI->image_lib->clear();
                                            
                                            $CI->picture_model->save_picture($newPath, $sideFolder, $where);
                                    

                                        }

                            }
                            else
                            {
                                //Error: Upload Failed
                                if (!isset($error_messages['errors'])) {
                                    $error_messages['errors'] = [];
                                }
                                $error_messages['errors'] = $CI->upload->display_errors('','');
                                
                            }
                }

            $extraVar++;
        }

        $full_path = 'assets/'.$mainFolder.'/full/'.$sideFolder;
        
        DeletePicturesANDFolder($full_path);

        //if there were no errors
        return $error_messages; 
         
        
    }

    public function SaveImagesResizeIfNeeded($sideFolder, $mainFolder, $where = 0, $inputFiles){
        $CI  =&get_instance();
        ini_set("memory_limit","256M");

        if (!file_exists('assets/'.$mainFolder)) {
           mkdir('assets/'.$mainFolder);
        }

        if (!file_exists('assets/'.$mainFolder.'/full')) {
            mkdir('assets/'.$mainFolder.'/full');
        }

        if (!file_exists('assets/'.$mainFolder.'/smaller')) {
        mkdir('assets/'.$mainFolder.'/smaller');
        }

        if (!file_exists('assets/'.$mainFolder.'/full/'.$sideFolder)) {
            mkdir('assets/'.$mainFolder.'/full/'.$sideFolder);
        }

        if (!file_exists('assets/'.$mainFolder.'/smaller/'.$sideFolder)) {
            mkdir('assets/'.$mainFolder.'/smaller/'.$sideFolder);
        }

        
        $error_messages = array();
    
        $config['upload_path'] = './assets/'.$mainFolder.'/full/'.$sideFolder;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '800000';

        $errors = [];
        $files = $inputFiles;
        
        $upload_count = count($files['userfile']['name'] );
   
        $extraVar = 0;
        $CI->load->library('image_lib');
        for( $i = 0; $i < $upload_count; $i++ )
        {
            date_default_timezone_set('UTC');
            $fileNameDate = date('Y-m-d-s');
            //get the extension pt1
            $ext = explode('.', $files['userfile']['name'][$i]);
            //get the extension pt2 (jpg stb)
            $actualExt = strtolower(end($ext));

            $fileName = $fileNameDate.$extraVar.".".$actualExt;

                $_FILES['userfile'] = [
                    'name'     => $fileName,
                    'type'     => $files['userfile']['type'][$i],
                    'tmp_name' => $files['userfile']['tmp_name'][$i],
                    'error'    => $files['userfile']['error'][$i],
                    'size'     => $files['userfile']['size'][$i]
                ];

                
                $CI->load->library('upload', $config);

                if ($files['userfile']['size'][$i] > 20000000) {
                     
                    if (!isset($error_messages['toobig'])) {
                       $error_messages['toobig'] = array();
                    }

                    array_push($error_messages['toobig'], $files['userfile']['name'][$i]);
                   
                    
                }else{

                            // Use upload library for file validation
                            if( $CI->upload->do_upload('userfile') )
                            {
                                //Upload was successful
                                $imageSrc = $CI->upload->data('full_path');
                            
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $imageSrc;
                                $config['create_thumb'] = FALSE;
                                $config['maintain_ratio'] = FALSE;
                                $config['quality'] = '100%';


                                $orgfile = $imageSrc;
                                $info = getimagesize($orgfile);
                                // echo "<pre>";
                                // print_r($info);

                                $width = $info[0];
                                $height = $info[1];
                            
                                        if ($width >= 2000 || $height >=2000) {
                                                
                                                $newwidth = 0;
                                                $newheight = 0;
                                
                                                if ($width > $height) {
                                                    // echo "szélesség a nagyobb";
                                                    $newwidth = 1200;
                                                    $szl = ($height / $width) *100;
                                                    // echo "szl: ".$szl;
                                                    $newheight = ($newwidth / 100) * $szl;
                                                    // echo "új mag: ".$newheight;
                                
                                                }else{
                                                    // echo "magasság a nagyobb";
                                                    $newheight = 1200;
                                                    
                                                    $szl = ($width / $height) * 100;
                                                    // echo "szl: ".$szl;
                                                    $newwidth = ($newheight / 100) * $szl;
                                                    // echo "új szél: ".$newwidth;
                                
                                                }

                                                $config['width'] = round($newwidth);
                                                $config['height'] = round($newheight);
                                                
                                                $newPath= './assets/'.$mainFolder.'/smaller/'.$sideFolder.'/'.$_FILES['userfile']['name'];
                                                $config['new_image'] = $newPath;
                                            
                                                $CI->image_lib->initialize($config);
                                                $CI->image_lib->resize();
                                                $CI->image_lib->clear();
                                                
                                                $CI->picture_model->save_picture($newPath, $sideFolder, $where);
                                            

                                        }else{
                                            //no resize needed
                              
                                            $newPath= './assets/'.$mainFolder.'/smaller/'.$sideFolder.'/'.$_FILES['userfile']['name'];
                                            $config['new_image'] = $newPath;
                                        
                                            $CI->image_lib->initialize($config);
                                            $CI->image_lib->resize();
                                            $CI->image_lib->clear();
                                            
                                            $CI->picture_model->save_picture($newPath, $sideFolder, $where);
                                    

                                        }

                            }
                            else
                            {
                                //Error: Upload Failed
                                if (!isset($error_messages['errors'])) {
                                    $error_messages['errors'] = [];
                                }
                                $error_messages['errors'] = $CI->upload->display_errors('','');
                                
                            }
                }

            $extraVar++;
        }

        $full_path = 'assets/'.$mainFolder.'/full/'.$sideFolder;
        DeletePicturesANDFolder($full_path);

        //if there were no errors
        return $error_messages; 
         
        
    }

     //1 is given back if the images are fine
     public function BiggerThanGivenMB($files){
          
        for ($i=0; $i < count($files['userfile']['size']); $i++) { 
            if($files['userfile']['size'][$i] > 20000000){
                return 0;
                
            }
           
        }

        return 1;
    }


    //1 is given back if there is at least one picture
    public function PostWithoutImage($files){
        if ($files['userfile']['size'][0] === 0) {
            return 0;
        }else{
            return 1;
        }
    }

    public function MoreThanOnePicture($files){
        if (count($files['userfile']['name']) != 1) {
            return 0;
        }else{
            return 1;
        }

    }


    private function GetTheHeightAndTheWidth($input){
        $CI  =&get_instance();
        $result = array(
            "info" => array(),
            "errors" => array()
        );
        // Use upload library for file validation
        if($CI->upload->do_upload($input) )
        {
        //Upload was successful
        $imageSrc = $CI->upload->data('full_path');
    
        $config['image_library'] = 'gd2';
        $config['source_image'] = $imageSrc;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '100%';


        $orgfile = $imageSrc;
        $info = getimagesize($orgfile);
            array_push($result['info'], $info[0]);
            array_push($result['info'], $info[1]);
        return $result;
        // echo "<pre>";
        //print_r($info);die;
        }else{
            //Error: Upload Failed
            $result['errors'] = $CI->upload->display_errors('','');
            return $result;

        }
    }
   
    private function VerticalHorizontal($important, $height, $width,$file_name,$mainFolder, $sideFolder){
        switch ($important) {
            case false:
                return 0;    
            break;
            
           case true:
                if ($height > $width) {
                    if (!isset($error['vertical'])) {
                            $error['vertical'] = [];
                    }
                    array_push($error['vertical'], $file_name);
                    
                    $full_path = 'assets/'.$mainFolder.'/full/'.$sideFolder.'/'.$_FILES['userfile']['name'];
                   // DeletePictureFromFolder($full_path);
                    return $error['vertical'];
                }else{
                    return 1;
                }
           break;
        }
    }
}

?>