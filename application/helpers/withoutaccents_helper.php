<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     function Withoutaccents($input){
        $hu=array('/é/','/É/','/á/','/Á/','/ó/','/Ó/','/ö/','/Ö/','/ő/','/Ő/','/ú/','/Ú/','/ű/','/Ű/','/ü/','/Ü/','/í/','/Í/','/ /','/,/','/-/');
        $en= array('e','E','a','A','o','O','o','O','o','O','u','U','u','U','u','U','i','I','_','_','_');
        $url=preg_replace($hu,$en,$input);

        return strtolower($url);
    }

?>